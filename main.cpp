#include "netlib.h"
#include "packet_builder.h"

#include <iostream>
#include <iomanip>
#include <ctime>
#include <sstream>
#include <thread>
#include <atomic>
#include <functional>

NetPeer netPeer;

void error_reporter(const char* error)
{
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    std::ostringstream oss;

    oss << std::put_time(&tm, "%A %B %d/%m/%Y %T");

    printf("[%s] %s\n", oss.str().c_str(), error);
}

void parsePacket(const std::string& packet, MachineAddress& sender)
{
    Packet recvStream(packet, error_reporter);

    int8_t tmpByteMsg = recvStream.ReadByte();
    std::string tmpMsg = recvStream.ReadString();

    printf("We got a message: %d %s\n from: %s\n", tmpByteMsg, tmpMsg.c_str(), sender.getAddress().c_str());
}

void listenthr()
{
    while(true)
    {
        netPeer.Listen(parsePacket);

        std::this_thread::sleep_for(std::chrono::milliseconds(0));
    }
}

void callbackEventHandler(uint8_t event, MachineAddress address)
{
    switch(event)
    {
    case PacketTypes::SERVER_ACCEPT_REQUEST:
        std::cout << "The server has accepted our request. Yipeee! We are now connected to " << address.getAddress() << std::endl;
        break;

    case PacketTypes::CLIENT_PING:
        //std::cout << "Client ping from " << address.getAddress() << " ! Elapsed Send Time: " << netPeer.GetClientStatistics(address).GetElapsedSendTime() << " Elapsed Recv Time: " << netPeer.GetClientStatistics(address).GetElapsedReceiveTime() << std::endl;
        break;

    case PacketTypes::SERVER_PING:
        //std::cout << "Server ping from " << address.getAddress() << " !" << std::endl;
        break;

    case PacketTypes::CLIENT_DISCONNECTION:
        std::cout << "Client disconnected!" << std::endl;

    default:
        break;
    }
}

int main()
{
    int mode;
    std::cout << "Enter the mode you want the application to act: 1. Client, 2. Server" << std::endl;
    std::cin >> mode;

    MachineAddress serverConnected;

    Packet packetStream(error_reporter);

    packetStream.WriteByte(0x4C);
    packetStream.Write("Hello and welcome!");

    errno_t iResult;

    netPeer.AssignCallbackFunction(callbackEventHandler);

    if(mode == 1)
    {
        netPeer.Initialize("tmplog_client.txt");
        iResult = netPeer.InitializeClient();

        if(iResult == 0)
        {
            printf("Client succesfully initialized!\n");
        }
        else printf("Client failed to initialize!\n");

        iResult = netPeer.Connect("127.0.0.1", 10292);

        if(iResult != 0) printf("Client failed to connect!\n");

        iResult = netPeer.BroadcastPacket(packetStream.getStream(), PACKET_FLAGS::FLAG_UNRELIABLE);

        if(iResult != 0) printf("Client failed to send the packet!\n");

        std::thread t2(listenthr);
        t2.join();

    }
    else if(mode == 2)
    {
        netPeer.Initialize("tmplog_server.txt");
        iResult = netPeer.InitializeServer(10292);

        if(iResult == 0)
        {
            printf("Server succesfully initialized!\n");
        }
        else printf("Server failed to initialize!\n");

        std::thread t1(listenthr);
        t1.join();
    }
    else
    {
        std::cout << "Unknown mode, try again.." << std::endl;
        main();
    }


    return 0;
}
