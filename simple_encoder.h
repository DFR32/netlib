#pragma once
#include <string>

namespace SimpleEncoder
{
    std::string encode(int a);
    std::string encode(float a);
    std::string encode(const std::string& str);
    std::string encode(int16_t a);
    std::string encode(int64_t a);

    int8_t encode(int8_t byte_to_encode);
    float decode_flt(const std::string& flt);
    int decode_int(const std::string& integ);
    int16_t decode_int16(const std::string& integ);
    int64_t decode_int64(const std::string& integ);

    std::string decode_str(const std::string& str);
    int8_t decode_byte(const std::string& str);
}
