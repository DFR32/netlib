#pragma once
#include "error_report.h"
#include "packet_types.h"
#include "packet_builder.h"
#include "utils.h"
#include "serializer.h"

#include <map>
#include <time.h>
#include <memory>

#ifndef errno_t
typedef int32_t errno_t;
#endif // errno_t


#if defined(_WIN32) || defined(_WIN32_WINNT) || defined(_WINNT_) || defined(WIN32)
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x501
#include <winsock2.h>
//#include <w32api.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <windows.h>
#else // defined
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#endif

// Define cross-compatible types
#if defined(_WIN32) || defined(_WIN32_WINNT) || defined(_WINNT_)
typedef SOCKET NetlibSocketRaw;
typedef sockaddr_in NetlibSockaddrIn;
typedef addrinfo NetlibAddrInfo;
typedef sockaddr NetLibSockAddrRaw;
#else
typedef int NetlibSocketRaw;
typedef sockaddr_in NetlibSockaddrIn;
typedef hostent NetlibAddrInfo;
typedef sockaddr NetLibSockAddrRaw;
#endif // defined

#define MAX_TRIES               5
#define MAX_CLIENTS             256
#define PADDING_SPACE_PACKET    20

#define MAX_SEND_UNIT			65507

//#define DEBUG_PINGS

// Enum to describe the type of connection we wish to establish
// 0 - PEER     (NOT USED YET)
// 1 - CLIENT
// 2 - SERVER
// 3 - UNKNOWN
enum CONNECTION_TYPE
{
    TYPE_PEER,
    TYPE_CLIENT,
    TYPE_SERVER,
    TYPE_UNKNOWN
};

// Enum to describe the status of the connection
// 0 - NOT CONNECTED
// 1 - CONNECTED
// 2 - DISCONNECTED
// 3 - RETRYING TO CONNECT
// 4 - KICKED
// 5 - SERVER NOT DEPLOYED
// 6 - SERVER DEPLOYED
// 7 - UNKNOWN
enum CONNECTION_STATUS
{
    CLIENT_NOT_CONNECTED,
    CLIENT_CONNECTED,
    CLIENT_DISCONNECTED,
    CLIENT_RETRYING,
    CLIENT_KICKED,
    SERVER_NOT_DEPLOYED,
    SERVER_DEPLOYED,
    STATUS_UNKNOWN
};

// Enum to describe the packet flags
// 0 - RELIABLE
// 1 - UNRELIABLE
enum PACKET_FLAGS
{
    FLAG_RELIABLE,
    FLAG_UNRELIABLE
};


// Enum to describe the plugin flags
// 0 - NONE
// 1 - GAIN CONTROL OF THE PACKET BEFORE SENDING
// 2 - GAIN CONTROL OF THE PACKET BEFORE BROADCASTING
// 4 - GAIN CONTROL OF THE PACKET BEFORE RECEIVING
// 8 - GAIN CONTROL OF THE CALLBACKS
// 16 - GAIN CONTROL OF THE CLIENT INITIALIZATION
// 32 - GAIN CONTROL OF THE CONNECTION STEP
// 64 - GAIN CONTROL OF THE SERVER INITIALIZATION
// 128 - GAIN CONTROL OF THE PACKET ASSEMBLY
enum PLUGINFLAGS
{
    FLAG_NONE = 0,
    FLAG_CONTROL_PACKET_SENDING = 1,
    FLAG_CONTROL_PACKET_BROADCASTING = 2,
    FLAG_CONTROL_PACKET_RECEIVING = 3,
    FLAG_CONTROL_CALLBACKS = 4,
    FLAG_CONTROL_INITIALIZATION_CLIENT = 5,
    FLAG_CONTROL_CONNECTION = 6,
    FLAG_CONTROL_INITIALIZATION_SERVER = 7,
    FLAG_CONTROL_PACKET_ASSEMBLY = 8
};

struct PLUGIN_FLAGS
{
    uint8_t NONE                           : 1;
    uint8_t CONTROL_PACKET_SENDING         : 1;
    uint8_t CONTROL_PACKET_BROADCASTING    : 1;
    uint8_t CONTROL_PACKET_RECEIVING       : 1;
    uint8_t CONTROL_CALLBACKS              : 1;
    uint8_t CONTROL_INITIALIZATION_CLIENT  : 1;
    uint8_t CONTROL_CONNECTION             : 1;
    uint8_t CONTROL_INITIALIZATION_SERVER  : 1;
    uint8_t CONTROL_PACKET_ASSEMBLY        : 1;

    PLUGIN_FLAGS()
            :
            NONE(0),
            CONTROL_PACKET_SENDING(0),
            CONTROL_PACKET_BROADCASTING(0),
            CONTROL_PACKET_RECEIVING(0),
            CONTROL_CALLBACKS(0),
            CONTROL_INITIALIZATION_CLIENT(0),
            CONTROL_CONNECTION(0),
            CONTROL_INITIALIZATION_SERVER(0),
            CONTROL_PACKET_ASSEMBLY(0)
    {
    }

    ~PLUGIN_FLAGS()
    {
        NONE                            = 0;
        CONTROL_PACKET_SENDING          = 0;
        CONTROL_PACKET_BROADCASTING     = 0;
        CONTROL_PACKET_RECEIVING        = 0;
        CONTROL_CALLBACKS               = 0;
        CONTROL_INITIALIZATION_CLIENT   = 0;
        CONTROL_CONNECTION              = 0;
        CONTROL_INITIALIZATION_SERVER   = 0;
        CONTROL_PACKET_ASSEMBLY         = 0;
    }
};

// Enum disconnect reasons
// 0 - KICK
// 1 - TIMEOUT
// 2 - QUIT
// 3 - CRASH
enum DISCONNECT_REASONS
{
    DISCONNECT_KICK,
    DISCONNECT_TIMEOUT,
    DISCONNECT_QUIT,
    DISCONNECT_CRASH
};

// Declare the netresolver namespace to fetch relevant information
// from the data structures
namespace NetResolver
{
    std::string GetIPFromSockaddr(NetlibAddrInfo* tmp);
    std::string GetIPFromSockaddr(NetLibSockAddrRaw t_raw);
    std::string GetIPFromSockaddr(NetlibSockaddrIn tmp);
    uint16_t GetPortFromSockaddr(NetlibSockaddrIn tmp);
}

// Declare the structure for holding the MTU size
struct AdapterSettings
{
    // Keep the MTU size in the largest possible (no) unsigned int
    // ...even though I probably need less than 16 bits...
    uint64_t MTUSize;

    AdapterSettings& operator= (const AdapterSettings& other) = default;
};

// Declare the machine address class which will serve as
// a neat wrapper to hold the socket addresses
class MachineAddress
{
    // Cross-compatible socket address type
    NetlibSockaddrIn socket_address;

    // Keep the IP address as a standard string
    std::string IP;

    // 16-bit data-type for the port
    uint16_t port;

public:

    // Machine address default constructor
    // Initializes port to 0
    MachineAddress() :
            socket_address(),
            IP(),
            port(0)
    {
    }

    // Constructor which takes a socket address wrapper type
    explicit MachineAddress(NetlibSockaddrIn tmp) :
            socket_address(tmp)
    {
        IP = NetResolver::GetIPFromSockaddr(tmp);
        port = NetResolver::GetPortFromSockaddr(tmp);
    }

    // Default destructor
    ~MachineAddress() = default;

    // Overload the assign operator
    MachineAddress& operator= (const MachineAddress& other)
    {
        if(this != &other)
        {
            this->socket_address = other.socket_address;
            this->IP = other.IP;
            this->port = other.port;
        }
        return *this;
    }

    // Overload the comparison operators (equal and less than)
    bool operator== (const MachineAddress& other) const
    {
#if defined(_WIN32) || defined(_WIN32_WINNT) || defined(_WINNT_)
        if((this->socket_address.sin_addr.s_addr == other.socket_address.sin_addr.s_addr ) && (socket_address.sin_port == other.socket_address.sin_port))
            return true;
#else
        if((socket_address.sin_addr.s_addr  == other.socket_address.sin_addr.s_addr ) && (socket_address.sin_port == other.socket_address.sin_port))
            return true;
#endif // defined

        return false;
    }

    bool operator< (const MachineAddress& other) const
    {
#if defined(_WIN32) || defined(_WIN32_WINNT) || defined(_WINNT_)
        if((this->socket_address.sin_addr.s_addr  < other.socket_address.sin_addr.s_addr ) && (socket_address.sin_port < other.socket_address.sin_port))
            return true;
#else
        if((socket_address.sin_addr.s_addr  == other.socket_address.sin_addr.s_addr ) && (socket_address.sin_port == other.socket_address.sin_port))
            return true;
#endif // defined

        return false;
    }

    // Function to return the IP address and port from the socket address
    std::string getAddress()
    {
        IP = NetResolver::GetIPFromSockaddr(socket_address);
        port = NetResolver::GetPortFromSockaddr(socket_address);

        std::string retStr;
        retStr += IP;
        retStr += ':';
        retStr += Utils::ToDecimalString((int)port);
        return retStr;
    }

    // Nifty type-cast operators below
    explicit operator NetlibSockaddrIn* ()
    {
        return &socket_address;
    }

    explicit operator NetLibSockAddrRaw* ()
    {
        return reinterpret_cast<NetLibSockAddrRaw*>(&socket_address);
    }

    // Return the address size of the socket address
    size_t getAddrSize()
    {
        NetLibSockAddrRaw sckaddr = *(reinterpret_cast<NetLibSockAddrRaw*>(&socket_address));

        return sizeof(sckaddr);
    }

    // Clear the machine address instance
    void clear()
    {
        // Set all bytes in the address range to 0
        memset(&this->socket_address, 0, sizeof(this->socket_address));

        // Clear the IP in the string
        this->IP.clear();

        // Set the port back to 0
        port = 0;
    }
};

// Class to keep track of the pings and time intervals
// Used for network statistics
class NetNodeStatistics
{
private:

    // Store the machine address of the recipient
    MachineAddress m_address;

    // Ping count (sent & received)
    size_t pings_sent;
    size_t pings_received;

    // Store the elapsed time (between send & receive)
    int64_t elapsed_time_between_pings_received;
    int64_t elapsed_time_between_pings_sent;

    // Store the latest timestamp of receive & send
    int64_t latest_ping_sent;
    int64_t latest_ping_received;

public:

    // Default constructor
    NetNodeStatistics() :
            m_address(),
            pings_sent(0),
            pings_received(0),
            elapsed_time_between_pings_received(0),
            elapsed_time_between_pings_sent(0),
            latest_ping_sent(Utils::GetCurrentSysTime()),
            latest_ping_received(Utils::GetCurrentSysTime())
    {
    }

    ~NetNodeStatistics() = default;

    // Clear and prepare for a reset
    void clear()
    {
        m_address = MachineAddress();

        pings_sent = 0;
        pings_received = 0;

        elapsed_time_between_pings_received = 0;
        elapsed_time_between_pings_sent = 0;

        latest_ping_sent = 0;
        latest_ping_received = 0;
    }

    // This function increases the pings that were received
    // ... and calcualtes the elapsed time between pings
    void IncreasePingsReceived()
    {
        auto time_now = Utils::GetCurrentSysTime();

        if(++pings_received <= 1)
        {
            elapsed_time_between_pings_received = 1;
            latest_ping_received = time_now;
            return;
        }

        elapsed_time_between_pings_received = (time_now - latest_ping_received);
        latest_ping_received = time_now;
    }

    // This function increases the pings that were sent
    // ... and calcualtes the elapsed time between pings
    void IncreasePingsSent()
    {
        auto time_now = Utils::GetCurrentSysTime();

        if(++pings_sent <= 1)
        {
            elapsed_time_between_pings_sent = 1;
            latest_ping_sent = time_now;
            return;
        }

        elapsed_time_between_pings_sent = (time_now - latest_ping_sent);
        latest_ping_sent = time_now;
    }

    // This calculates the latency by doing a weighted average
    // ... on the elapsed times between last received and sent packets
    int64_t GetLatency()
    {
        if(pings_sent > 0 && pings_received > 0)
            return ((elapsed_time_between_pings_received + elapsed_time_between_pings_sent) / 2);
        return 0;
    }

    // This function increases returns the elapsed send time
    int64_t GetElapsedSendTime()
    {
        if(pings_sent > 0)
            return elapsed_time_between_pings_sent;
        return 0;
    }

    // This function increases returns the elapsed received time
    int64_t GetElapsedReceiveTime()
    {
        if(pings_received > 0)
            return elapsed_time_between_pings_received;
        return 0;
    }

    // This function increases returns the last send time
    int64_t GetLastSendTime()
    {
        if(pings_sent > 0)
            return latest_ping_sent;
        return 0;
    }

    // This function increases returns the last receive time
    int64_t GetLastReceiveTime()
    {
        if(pings_received > 0)
            return latest_ping_received;
        return 0;
    }

    // This function increases returns the packets sent counter
    size_t GetPingsSent()
    {
        return pings_sent;
    }

    // This function increases returns the packets received counter
    size_t GetPingsReceived()
    {
        return pings_received;
    }
};

// Declare the netlib plugin class
// Supposedly virtual
class NetlibPlugin
{
    // Specify that NetPeer is a friend
    friend class NetPeer;

    // Keep some plugin information such as
    // ... plugin name and author
    std::string plugin_name;
    std::string plugin_author;

    // Define the plugin version (HI & LOW)
    uint8_t plugin_version_high;
    uint8_t plugin_version_low;

    // Keep a UID which should distinguish
    // ... the plugin from others
    uint16_t unique_identifier;

    // Define the plugin flags which determine
    // ... which parts are altered by the plugin
    PLUGIN_FLAGS plugin_flags;

public:

    // Default constructor
    NetlibPlugin() :
            plugin_name("NONE"),
            plugin_author("NONE"),
            plugin_version_high(1),
            plugin_version_low(0),
            plugin_flags()
    {
        // Calculate a random number as the unique identifier
        // Need a better RNG and better seeding
        unique_identifier = static_cast<uint16_t>(rand() % 65535);
    }

    // Custom constructor which takes as parameters the plugin information
    // ... and the flags
    NetlibPlugin(std::string&& name, std::string&& author, uint8_t version_high, uint8_t version_low) :
            plugin_name(name),
            plugin_author(author),
            plugin_version_high(version_high),
            plugin_version_low(version_low),
            plugin_flags()
    {
        // Calculate a random number as the unique identifier
        // Need a better RNG and better seeding
        unique_identifier = static_cast<uint16_t>(rand() % 65535);
    }

    void set_flag(uint8_t flag_opt, uint8_t setting)
    {
        switch(flag_opt)
        {
            case FLAG_NONE:
                plugin_flags.NONE                           = setting;
                break;

            case FLAG_CONTROL_PACKET_SENDING:
                plugin_flags.CONTROL_PACKET_SENDING         = setting;
                break;

            case FLAG_CONTROL_PACKET_BROADCASTING:
                plugin_flags.CONTROL_PACKET_BROADCASTING    = setting;
                break;

            case FLAG_CONTROL_PACKET_RECEIVING:
                plugin_flags.CONTROL_PACKET_RECEIVING        = setting;
                break;

            case FLAG_CONTROL_CALLBACKS:
                plugin_flags.CONTROL_CALLBACKS               = setting;
                break;

            case FLAG_CONTROL_INITIALIZATION_CLIENT:
                plugin_flags.CONTROL_INITIALIZATION_CLIENT   = setting;
                break;

            case FLAG_CONTROL_CONNECTION:
                plugin_flags.CONTROL_CONNECTION               = setting;
                break;

            case FLAG_CONTROL_INITIALIZATION_SERVER:
                plugin_flags.CONTROL_INITIALIZATION_SERVER    = setting;
                break;

            case FLAG_CONTROL_PACKET_ASSEMBLY:
                plugin_flags.CONTROL_PACKET_ASSEMBLY          = setting;
                break;

            default:
                plugin_flags.NONE                           = setting;
                break;
        }
    }

    uint8_t get_flag(uint8_t flag_opt)
    {
        switch(flag_opt)
        {
            case FLAG_NONE:
                return plugin_flags.NONE;

            case FLAG_CONTROL_PACKET_SENDING:
                return plugin_flags.CONTROL_PACKET_SENDING;

            case FLAG_CONTROL_PACKET_BROADCASTING:
                return plugin_flags.CONTROL_PACKET_BROADCASTING;

            case FLAG_CONTROL_PACKET_RECEIVING:
                return plugin_flags.CONTROL_PACKET_RECEIVING;

            case FLAG_CONTROL_CALLBACKS:
                return plugin_flags.CONTROL_CALLBACKS;

            case FLAG_CONTROL_INITIALIZATION_CLIENT:
                return plugin_flags.CONTROL_INITIALIZATION_CLIENT;

            case FLAG_CONTROL_CONNECTION:
                return plugin_flags.CONTROL_CONNECTION;

            case FLAG_CONTROL_INITIALIZATION_SERVER:
                return plugin_flags.CONTROL_INITIALIZATION_SERVER;

            case FLAG_CONTROL_PACKET_ASSEMBLY:
                return plugin_flags.CONTROL_PACKET_ASSEMBLY;

            default:
                return plugin_flags.NONE;
        }
    }

    // Virtual destructor to allow destruction of
    // ... inheriting instances
    virtual ~NetlibPlugin() = default;

    // Define the is_equal operator to compare with
    // ...other NetlibPlugin instances
    bool operator== (NetlibPlugin& other) const
    {
        return (this->unique_identifier == other.unique_identifier);
    }

    // BELOW IS A LIST OF FUNCTIONS THAT ARE DEFINED TO RETURN 0
    // THESE ARE OVERWRITTEN BY DERIVING CLASSES
    // DO NOT MODIFY
    virtual errno_t alterPacketBeforeSending(MachineAddress& addr, std::string& packet, PACKET_FLAGS& p_flags)
    {
        return 0;
    }

    virtual errno_t alterPacketBeforeBroadcast(MachineAddress& addr, std::string& packet, PACKET_FLAGS& p_flags)
    {
        return 0;
    }

    virtual errno_t alterInitializationClient()
    {
        return 0;
    }

    virtual errno_t alterConnection(MachineAddress& server_address)
    {
        return 0;
    }

    virtual errno_t alterInitializationServer(uint16_t& server_port)
    {
        return 0;
    }

    virtual errno_t alterPacketListening(MachineAddress& addr, std::string& packet)
    {
        return 0;
    }


    virtual errno_t alterCallbacks(MachineAddress& m_address, Packet& packet, int8_t& callback_type)
    {
        return 0;
    }

    virtual errno_t alterPacketAssembly(const std::string& packet)
    {
        return 0;
    }

};


// Define the NetPeer class
class NetPeer
{
    // Specify NetlibPlugin as a friend class
    // to access it's private members
    friend class NetlibPlugin;

    // Define a FileLogger instance which acts as an
    // ... error reporter
    FileLogger log;

    // Define the connectionType attribute to keep track of
    // ... the type of the connection that we are dealing with
    // (see CONNECTION_TYPE enum for more information)
    CONNECTION_TYPE connectionType;

    // Define the connectionStatus attribute to keep track of
    // ... the current state of the connection
    // (see CONNECTION_STATUS enum for more information)
    CONNECTION_STATUS connectionStatus;

    // Pointer to mess with socket data here and there
    void* socketData;

    // Wrapper types for cross-compatibility
    NetlibSocketRaw cSocket;

    NetlibAddrInfo winHints;
    NetlibAddrInfo* winResult;

    // AdapterSettings instance to retrieve
    // ... the MTU size and other information
    AdapterSettings adapterSettings;

    // The port that the server will be listening on
    uint16_t listening_port;

    // The machine address of the server in the wrapper class
    MachineAddress server_address;

    // A vector to keep track of the addresses of the clients
    // ... that are currently connected
    std::vector<MachineAddress> client_addresses;

    // A map which keeps track of which NetNodeStatistics corresponds to which
    // ... MachineAddress instance (useful for keeping track of network statistics)
    std::map<MachineAddress, NetNodeStatistics> client_statistics;

    // The NetNodeStatistics of the server
    NetNodeStatistics server_statistics;

    // A function wrapper for the event callback which has to
    // ... be fed in by the user of the library
    std::function<void(uint8_t, MachineAddress)> eventCallback;

    // A vector which should contain all the current loaded
    // ... NetlibPlugin instances
    std::vector<std::unique_ptr<NetlibPlugin> > plugin_list;

public:
    // Default constructor
    NetPeer() :
            log(),
            connectionType(CONNECTION_TYPE::TYPE_UNKNOWN),
            connectionStatus(CONNECTION_STATUS::STATUS_UNKNOWN),
            socketData(nullptr),
            winHints(),
            winResult(nullptr),
            adapterSettings(),
            listening_port(0),
            server_address(),
            client_addresses(),
            client_statistics(),
            server_statistics(),
            eventCallback(),
            plugin_list()
    {
        // Initialize the socket and feed the standard
        // ... random number generator a seed equivalent to the time
        // ... since the beginning of the unix epoch
#if defined(_WIN32) || defined(_WINNT_) || defined(_WIN32_WINNT)
        cSocket = INVALID_SOCKET;
#else
        // to do
#endif // defined

        srand(static_cast<unsigned int>(time(nullptr)));
    }

    // Customized default destructor
    ~NetPeer()
    {
#if defined(_WIN32) || defined(_WINNT_) || defined(_WIN32_WINNT)
        closesocket(cSocket);
        WSACleanup();
#else

#endif // defined

        // Reset values to default
        socketData = nullptr;
        listening_port = 0;

        // Clear the used data
        server_address = MachineAddress();
        client_addresses.clear();
        client_statistics.clear();
        server_statistics.clear();
        server_address.clear();
        plugin_list.clear();
    }

    // Function which feds the error report
    // ... file logger the path of the file which
    // ... will contain the debugging messages
    void Initialize(const char* path)
    {
        log.Initialize(path);
    }

    // Assign operator
    NetPeer & operator= (const NetPeer & other)
    {
        // Check against self, if not, assign properties
        if(this != & other)
        {
            this->log = other.log;
            this->connectionType = other.connectionType;
            this->connectionStatus = other.connectionStatus;
            this->socketData = other.socketData;
            this->cSocket = other.cSocket;

            this->winHints = other.winHints;
            this->winResult = other.winResult;
            this->adapterSettings = other.adapterSettings;

            this->listening_port = other.listening_port;

            this->server_address = other.server_address;
            this->client_addresses = other.client_addresses;

            this->eventCallback = other.eventCallback;
        }

        return *this;
    }

    // Function which retrieves important adapter settings
    // ... such as the MTU size
    errno_t PopulateAdapterSettings()
    {
#if defined(_WIN32) || defined(_WINNT_) || defined(_WIN32_WINNT) || defined(WIN32)
        PIP_ADAPTER_ADDRESSES pAddress;
        ULONG outBufLen = 0;
        outBufLen = 15000;
        ULONG uRetVal;
        uint8_t iterations = 0;

        // Do some windows magic over here
        do
        {
            pAddress = (IP_ADAPTER_ADDRESSES*) HeapAlloc(GetProcessHeap(), 0, outBufLen);

            // Treat the case when HeapAlloc fails to allocate memory
            if(pAddress == nullptr)
            {
                log.write("PopulateAdapterSettings() -> HeapAlloc failed to allocate memory for IP_ADAPTER_ADDRESSES struct.");
                return -1;
            }

            uRetVal = GetAdaptersAddresses(AF_INET, GAA_FLAG_INCLUDE_PREFIX, nullptr, pAddress, &outBufLen);

            // Treat the overflow case (should probably treat other errors aswell?)
            if(uRetVal == ERROR_BUFFER_OVERFLOW)
            {
                HeapFree(GetProcessHeap(), 0, pAddress);
                pAddress = nullptr;
            }

            ++iterations;
        } while((uRetVal == ERROR_BUFFER_OVERFLOW) && (iterations < MAX_TRIES));

        if(uRetVal != NO_ERROR)
        {
            log.write("PopulateAdapterSettings() -> GetAdaptersAddresses failed with error code: %d\n", uRetVal);
            return (errno_t)uRetVal;
        }

        // Assign the MTU size that was just retrieved
        adapterSettings.MTUSize = pAddress->Mtu;

        // Free the memory if it was used
        if(pAddress)
            HeapFree(GetProcessHeap(), 0, pAddress);

        return 0;
#else
#endif // defined
    }

    // Function which assigns a callback function to the class instance
    errno_t AssignCallbackFunction(const std::function<void(uint8_t, MachineAddress)>& callbackFunc)
    {
        this->eventCallback = callbackFunc;

        return 0;
    }

    // Function which registers a plugin and allows it to alter
    // ... the behaviour of the library (different flags alter different functions)
    errno_t RegisterPlugin(std::unique_ptr<NetlibPlugin>& plugin)
    {
        this->plugin_list.push_back(std::move(plugin));

        return 0;
    }

    // Function to initialize the client socket with the minimum
    // ... required information
    errno_t InitializeClient()
    {
#if defined(_WIN32) || defined(_WINNT_) || defined(_WIN32_WINNT)
        WSAData wsaData = {};
        int32_t iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);

        if(iResult != 0)
        {
            log.write("InitializeClient -> WSAStartup failed: %d", iResult);
            return iResult;
        }

        // Get the adapter settings and treat any errors
        errno_t adapterState = PopulateAdapterSettings();

        if(adapterState != 0)
        {
            log.write("InitializeClient -> there has been an error in populating the adapter settings. Error: %d", adapterState);
            return adapterState;
        }
#else // not defined

#endif

        connectionStatus = CONNECTION_STATUS::CLIENT_NOT_CONNECTED;
        connectionType = CONNECTION_TYPE::TYPE_CLIENT;

        if(plugin_list.empty())
            return 0;

        for(auto& plugin : plugin_list)
        {
            if((plugin->get_flag(FLAG_CONTROL_INITIALIZATION_CLIENT)))
                plugin->alterInitializationClient();
        }

        return 0;
    }

    // Function used to connect the client to a server with a
    // ... given IP and port
    errno_t Connect(const char* IP, uint32_t port)
    {
#if defined(_WIN32) || defined(_WINNT_) || defined(_WIN32_WINNT)
        ZeroMemory(&winHints, sizeof(winHints));
        winHints.ai_family = AF_UNSPEC;
        winHints.ai_socktype = SOCK_DGRAM;
        winHints.ai_protocol = IPPROTO_UDP;

        // Get information about the address and store it in the
        // ... winHints and winResult structures
        int32_t iResult = getaddrinfo(IP, Utils::ToDecimalString(port).c_str(), &winHints, &winResult);

        if (iResult != 0)
        {
            log.write("Connect -> getaddrinfo failed with error: %d", iResult);
            return iResult;
        }

        // Try to construct the socket with a valid address
        // Iterate all hints that resulted from the call
        struct addrinfo *ptr = nullptr;
        for(ptr = winResult; ptr != nullptr; ptr = ptr->ai_next)
        {
            cSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
            if(cSocket == INVALID_SOCKET)
            {
                log.write("Connect -> socket constructor failed with fatal error: %ld", WSAGetLastError());
                freeaddrinfo(winResult);
                WSACleanup();
                return WSAGetLastError();
            }

            iResult = connect( cSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
            if(iResult == SOCKET_ERROR)
            {
                closesocket(cSocket);
                cSocket = INVALID_SOCKET;
                log.write("Connect -> could not connect to address [%s], attempting next address...", NetResolver::GetIPFromSockaddr(ptr).c_str());
                continue;
            }
            break;
        }

        // Free the address info of winResult
        freeaddrinfo(winResult);

        // Treat more error cases...
        if (cSocket == INVALID_SOCKET)
        {
            log.write("Connect -> unable to connect to the server.");
            server_address = MachineAddress();
            WSACleanup();
            return -1;
        }

        // send connection request, to-do
        SendConnectionRequest();

#else // not defined

#endif

        if(plugin_list.empty())
            return 0;

        // Loop through the plugins and pass the control flow
        // ... before our gracious exit
        for(auto& plugin : plugin_list)
        {
            if((plugin->get_flag(FLAG_CONTROL_CONNECTION)))
                plugin->alterConnection(server_address);
        }

        return 0;
    }

    // Function to retrieve the server address
    MachineAddress GetServerAddress()
    {
#if defined(_WIN32) || defined(_WINNT_) || defined(_WIN32_WINNT)
        if(connectionStatus != CONNECTION_STATUS::CLIENT_CONNECTED)
        {
            log.write("GetServerAddress -> cannot return a server address if the client is not connected.");
            return MachineAddress();
        }

        return server_address;
#else
#endif // defined
    }

    // Function to return the client address at the given position
    // ... in the client_addresses vector
    MachineAddress GetClientAddress(size_t clientID)
    {
        return client_addresses.at(clientID);
    }

    // Function used to return the client statistics of the machine address
    // ... that is associated with it
    NetNodeStatistics GetClientStatistics(const MachineAddress& m_address)
    {
        return client_statistics[m_address];
    }

    // Function used to initialize the server with the given port
    // Will set the SERVER_DEPLOYED flag if successful
    errno_t InitializeServer(uint16_t port)
    {
        connectionStatus = SERVER_NOT_DEPLOYED;
#if defined(_WIN32) || defined(_WINNT_) || defined(_WIN32_WINNT)
        WSAData wsaData = {};

        // Initialize WSA (Windows Sockets Application)
        int32_t iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);

        // Error checking...
        if(iResult != 0)
        {
            log.write("InitializeServer -> WSAStartup failed: %d", iResult);
            return iResult;
        }

        ZeroMemory(&winHints, sizeof(winHints));
        winHints.ai_family = AF_INET;           // IPv4
        winHints.ai_socktype = SOCK_DGRAM;      // UDP
        winHints.ai_protocol = IPPROTO_UDP;     // UDP
        winHints.ai_flags = AI_PASSIVE;         // Requires manual binding

        // Get the address info and try to resolve
        iResult = getaddrinfo(nullptr, Utils::ToDecimalString(port).c_str(), &winHints, &winResult);
        if ( iResult != 0 ) {
            log.write("InitializeServer -> getaddrinfo failed with error: %d", iResult);
            log.write("InitializeServer -> could not resolve server.");
            WSACleanup();
            return iResult;
        }

        // Create the socket and prepare to bind
        cSocket = socket(winResult->ai_family, winResult->ai_socktype, winResult->ai_protocol);
        if(cSocket == INVALID_SOCKET)
        {
            log.write("InitializeServer -> socket creation failed, socket is invalid.");
            log.write("InitializeServer -> error is: %ld", WSAGetLastError());
            freeaddrinfo(winResult);
            WSACleanup();
            return WSAGetLastError();
        }

        // Bind and prepare to listen
        iResult = bind( cSocket, winResult->ai_addr, static_cast<int>(winResult->ai_addrlen));
        if(iResult == SOCKET_ERROR)
        {
            log.write("InitializeServer -> bind failed with error: %d", WSAGetLastError());
            freeaddrinfo(winResult);
            closesocket(cSocket);
            WSACleanup();
            return WSAGetLastError();
        }

        // Free the used memory
        freeaddrinfo(winResult);

        // Fetch the adapter settings
        errno_t adapterState = PopulateAdapterSettings();

        // More error checking
        if(adapterState != 0)
        {
            log.write("InitializeClient -> there has been an error in populating the adapter settings. Error: %d", adapterState);
            return adapterState;
        }
#else // not defined

#endif

        // Set the connection type to TYPE_SERVER
        // ... and the status to SERVER_DEPLOYED
        connectionType = CONNECTION_TYPE::TYPE_SERVER;
        connectionStatus = SERVER_DEPLOYED;

        // Set the port to the one that we are listening on
        listening_port = port;

        if(plugin_list.empty())
            return 0;

        // Pass execution control flow to the plugins
        for(auto& plugin : plugin_list)
        {
            if((plugin->get_flag(FLAG_CONTROL_INITIALIZATION_SERVER)))
                plugin->alterInitializationServer(listening_port);
        }
        return 0;
    }

    // Function used by either the client or the server
    // ... to broadcast a packet
    // For the server, the function sends the packet to every client
    // For the client, the function sends the packet to the server
    errno_t BroadcastPacket(const std::string& packet, PACKET_FLAGS packet_flag)
    {
        #if defined(_WIN32) || defined(_WINNT_) || defined(_WIN32_WINNT)
        int32_t ret;
        size_t packet_size = packet.size();
        size_t final_size = 0;

        if( (packet_size + PADDING_SPACE_PACKET) > adapterSettings.MTUSize)
        {
            log.write("BroadcastPacket -> Functionality not yet implemented for splitting messages.");
            return -1;
        }

        // Construct a packet with the mandatory end-points
        std::string tmp;

        tmp += (char)PacketSegments::PACKET_BEGIN;
        tmp += packet;
        tmp += (char)PacketSegments::PACKET_END;


        // Pass execution control flow to the plugins
        if(!plugin_list.empty())
        {
            for(auto& plugin : plugin_list)
            {
                if((plugin->get_flag(FLAG_CONTROL_PACKET_ASSEMBLY)))
                    plugin->alterPacketAssembly(tmp);
            }
        }

        std::vector<uint8_t> data_to_send(tmp.size(), 0);

        // Handle the different types of broadcasting that we have to
        // ... deal with based on the type of connection that we have
        // ... established
        if(connectionType == CONNECTION_TYPE::TYPE_CLIENT)
        {
            switch(packet_flag)
            {
                case PACKET_FLAGS::FLAG_UNRELIABLE:
                    if(!plugin_list.empty())
                    {
                        for(auto& plugin : plugin_list)
                        {
                            if((plugin->get_flag(FLAG_CONTROL_PACKET_BROADCASTING)))
                                plugin->alterPacketBeforeBroadcast(server_address, tmp, packet_flag);
                        }
                    }

                    if(data_to_send.size() != tmp.size())
                        data_to_send.resize(tmp.size(), 0);

                #ifdef EXTREME_VERBOSE
                printf("Sending: ");
                for(size_t i = 0; i < tmp.size(); ++i)
                {
                    printf("%d ", tmp[i]);
                }
                printf("\n");
                #endif

                    for(uint64_t itt = 0; itt < tmp.size(); ++itt)
                    {
                        data_to_send.at(itt) = (uint8_t)tmp[itt];
                    }

                    data_to_send = serialize(data_to_send);

                    final_size = Bounds::clamp<size_t>(tmp.size(), 0, MAX_SEND_UNIT);

                    if (final_size != tmp.size())
                    {
                        log.write("BroadcastPacket -> Asked to send a packet that was larger than UDP data size.");
                        return -1;
                    }

                    ret = send(cSocket, reinterpret_cast<char*>(data_to_send.data()), static_cast<int>(final_size), 0);

                    if(ret == SOCKET_ERROR)
                    {
                        log.write("BroadcastPacket -> send failed with error: %d", WSAGetLastError());
                        return WSAGetLastError();
                    }
                    return 0;

                case PACKET_FLAGS::FLAG_RELIABLE:
                    if(!plugin_list.empty())
                    {
                        for(auto& plugin : plugin_list)
                        {
                            if((plugin->get_flag(FLAG_CONTROL_PACKET_BROADCASTING)))
                                plugin->alterPacketBeforeBroadcast(server_address, tmp, packet_flag);
                        }
                    }

                    if(data_to_send.size() != tmp.size())
                        data_to_send.resize(tmp.size(), 0);

                    log.write("BroadcastPacket -> Functionality not yet implemented for reliable packets.");
                    return -1;

                default:
                    log.write("BroadcastPacket -> function invoked with improper packet flag, packet flag used in the call was: %d", (int)packet_flag);
                    return -1;
            }
        }
        else if(connectionType == CONNECTION_TYPE::TYPE_SERVER)
        {
            switch(packet_flag)
            {
                case PACKET_FLAGS::FLAG_UNRELIABLE:
                    for(MachineAddress & macAddress : this->client_addresses)
                    {
                        if(!plugin_list.empty())
                        {
                            for(auto& plugin : plugin_list)
                            {
                                if((plugin->get_flag(FLAG_CONTROL_PACKET_BROADCASTING)))
                                    plugin->alterPacketBeforeBroadcast(macAddress, tmp, packet_flag);
                            }
                        }

                        if(data_to_send.size() != tmp.size())
                            data_to_send.resize(tmp.size(), 0);

                        ((NetLibSockAddrRaw*)macAddress)->sa_family = AF_INET;

                        for(uint64_t itt = 0; itt < tmp.size(); ++itt)
                        {
                            data_to_send.at(itt) = (uint8_t)tmp[itt];
                        }

                        data_to_send = serialize(data_to_send);

                        final_size = Bounds::clamp<size_t>(tmp.size(), 0, MAX_SEND_UNIT);

                        if (final_size != tmp.size())
                        {
                            log.write("BroadcastPacket -> Asked to send a packet that was larger than UDP data size.");
                            return -1;
                        }

                        ret = sendto(cSocket, reinterpret_cast<char*>(data_to_send.data()), static_cast<int>(final_size), 0, (NetLibSockAddrRaw*)macAddress, static_cast<int>(macAddress.getAddrSize()));


                        if(ret == SOCKET_ERROR)
                        {
                            log.write("BroadcastPacket -> send failed with error: %d", WSAGetLastError());
                            return WSAGetLastError();
                        }
                    }
                    return 0;

                case PACKET_FLAGS::FLAG_RELIABLE:
                    log.write("BroadcastPacket -> Functionality not yet implemented for reliable packets.");

                    for(MachineAddress & macAddress : this->client_addresses)
                    {
                        if(!plugin_list.empty())
                        {
                            for(auto& plugin : plugin_list)
                            {
                                if((plugin->get_flag(FLAG_CONTROL_PACKET_BROADCASTING)))
                                    plugin->alterPacketBeforeBroadcast(macAddress, tmp, packet_flag);
                            }

                            if(data_to_send.size() != tmp.size())
                                data_to_send.resize(tmp.size(), 0);
                        }
                    }
                    return -1;

                default:
                    log.write("BroadcastPacket -> function invoked with improper packet flag, packet flag used in the call was: %d", (int)packet_flag);
                    return -1;
            }
        }
        return 0;
#else

#endif // defined
    }

    // A more specific packet broadcasting method that only sends the packet to a certain
    // ... MachineAddress instance which will be the only recepient
    // Documentation is similar to the BroadcastPacket method
    errno_t SendPacket(const std::string& packet, MachineAddress& destination/*destination??*/, PACKET_FLAGS packet_flag )
    {
        #if defined(_WIN32) || defined(_WINNT_) || defined(_WIN32_WINNT)
        int32_t ret;
        size_t packet_size = packet.size();
        size_t final_size = 0;

        if((packet_size + PADDING_SPACE_PACKET) > adapterSettings.MTUSize)
        {
            log.write("SendPacket -> Functionality not yet implemented for splitting messages.");
            return -1;
        }

        std::string tmp;
        tmp += (char)PacketSegments::PACKET_BEGIN;
        tmp += packet;
        tmp += (char)PacketSegments::PACKET_END;

        if(!plugin_list.empty())
        {
            for(auto& plugin : plugin_list)
            {
                if((plugin->get_flag(FLAG_CONTROL_PACKET_ASSEMBLY)))
                    plugin->alterPacketAssembly(tmp);
            }
        }

        std::vector<uint8_t> data_to_send(tmp.size(), 0);

        switch(packet_flag)
        {
            case PACKET_FLAGS::FLAG_UNRELIABLE:
                if (!plugin_list.empty())
                {
                    for (auto& plugin : plugin_list)
                    {
                        if ((plugin->get_flag(FLAG_CONTROL_PACKET_SENDING)))
                            plugin->alterPacketBeforeSending(destination, tmp, packet_flag);
                    }
                }

                if (data_to_send.size() != tmp.size())
                    data_to_send.resize(tmp.size(), 0);

                ((NetLibSockAddrRaw*)destination)->sa_family = AF_INET;

                for (uint64_t itt = 0; itt < tmp.size(); ++itt)
                {
                    data_to_send.at(itt) = (uint8_t)tmp[itt];
                }

                data_to_send = serialize(data_to_send);

                final_size = Bounds::clamp<size_t>(tmp.size(), 0, MAX_SEND_UNIT);

                if (final_size != tmp.size())
                {
                    log.write("SendPacket -> Asked to send a packet that was larger than UDP data size.");
                    return -1;
                }

                ret = sendto(cSocket, reinterpret_cast<char*>(data_to_send.data()), static_cast<int>(final_size), 0, (NetLibSockAddrRaw*)destination, static_cast<int>(destination.getAddrSize()));

                if (ret == SOCKET_ERROR)
                {
                    log.write("SendPacket -> sendto failed with error: %d", WSAGetLastError());
                    return WSAGetLastError();
                }
                return 0;

            case PACKET_FLAGS::FLAG_RELIABLE:
                if(!plugin_list.empty())
                {
                    for(auto& plugin : plugin_list)
                    {
                        if((plugin->get_flag(FLAG_CONTROL_PACKET_SENDING)))
                            plugin->alterPacketBeforeSending(destination, tmp, packet_flag);
                    }
                }

                log.write("SendPacket -> Functionality not yet implemented for reliable packets.");
                return -1;

            default:
                log.write("SendPacket -> function invoked with improper packet flag, packet flag used in the call was: %d", (int)packet_flag);
                return -1;
        }
        return 0;
#else

#endif // defined
    }

    // Function that should be called by the user in the library to handle the listening of packets
    // The function that is passed is the function that is called when an unknown packet is received
    errno_t Listen(const std::function<void(const std::string&, MachineAddress&)>& func)
    {
#if defined(_WIN32) || defined(_WINNT_) || defined(_WIN32_WINNT)
        NetlibSockaddrIn tmpPeerAddr;
        int SenderAddrSize = sizeof(tmpPeerAddr);

        // Create the buffer that should store the received bytes
        // Set a limit (currently 2048)
        char buf[2048] = {'\0'};

        // Receive the bytes from the windows buffer
        int32_t iResult = recvfrom(cSocket, buf, 2048, 0, (struct sockaddr*)&tmpPeerAddr, &SenderAddrSize);

        // Error checking...
        if(iResult == SOCKET_ERROR)
        {
            log.write("Listen -> socket error occured when calling recvfrom. error: %d", WSAGetLastError());
            return WSAGetLastError();
        }

        // Construct a machine address based on the socket address wrapper
        MachineAddress tmpMAddr = MachineAddress(tmpPeerAddr);

#ifdef EXTREME_VERBOSE
        log.write("Listen -> we received a packet from %s", tmpMAddr.getAddress().c_str());

        printf("Packet contents: ");
        log.write("Listen -> Packet contents (BUF): ");
        for(int32_t kl = 0; kl < iResult; ++kl)
        {
            printf("%d ", buf[kl]);
            log.write("Listen -> %d", buf[kl]);
        }
        printf("\n");
#endif

        std::vector<uint8_t> received_data(iResult, 0);

        for(int32_t itt = 0; itt < iResult; ++itt)
        {
            received_data.at((uint32_t)itt) = (uint8_t)buf[itt];
        }

        received_data = deserialize(received_data);

        // Construct the standardized buffer (IMPORTANT: FEED IT THE SIZE!)
        std::string stded_buf((const char*)received_data.data(), received_data.size());

        // Pass execution control flow to the plugins
        if(!plugin_list.empty())
        {
            for(auto& plugin : plugin_list)
            {
                if((plugin->get_flag(FLAG_CONTROL_PACKET_RECEIVING)))
                    plugin->alterPacketListening(tmpMAddr, stded_buf);
            }
        }

        #ifdef EXTREME_VERBOSE
        log.write("Packet start: %d & packet_end: %d", stded_buf[0], stded_buf[iResult - 1]);
        #endif
        // Check the packet to see if it matches our begin&end criteria
        if((stded_buf[0] == PacketSegments::PACKET_BEGIN) && (stded_buf[iResult - 1] == PacketSegments::PACKET_END)) {
            // Do a clean slice
            std::string packetStream = Utils::slicex(&stded_buf[0], (uint32_t) iResult, 1, (uint32_t) iResult - 1);

            // Construct a packet based on the buffer that it was fed
            Packet tmpPacket(packetStream);

            // Analyze the packet and proof-read it for errors
            int8_t packet_analyze_result = tmpPacket.VerifyIntegrity();
            if (packet_analyze_result != 3 /* VALID CONSISTENCY */ ) {
                // Error messages that some insightful minds might find useful
                log.write("Listen -> packet received with invalid integrity, inconsistency error %d.",
                          packet_analyze_result);

                log.write("Listen -> Packet contents (INTEGR) [packetStream]: ");
                for (size_t i = 0; i < packetStream.size(); ++i) {
                    log.write("Listen -> %d", packetStream[i]);
                }

                log.write("Listen -> Packet contents (INTEGR) [stded_buf]: ");
                for (size_t i = 0; i < stded_buf.size(); ++i) {
                    log.write("Listen -> %d", packetStream[i]);
                }

                log.write("Listen -> Packet contents (INTEGR) [buf]: ");
                for (int32_t i = 0; i < iResult; ++i) {
                    log.write("Listen -> %d", buf[i]);
                }

                return -1;
            }

            // Read the first byte of the packet
            auto packByte = (uint8_t) tmpPacket.ReadByte();

#ifdef EXTREME_VERBOSE
            log.write("Pack byte is %d", packByte);
#endif

            // Process known messages and pass the rest to the
            // ... user-defined callback handler
            if (packByte < 50)
            {
                // Deal with the known stuff
                this->ProcessPacket(packetStream, tmpMAddr);
            } // Treat the off-the-beaten-track signals...
            else
            {
                func(packetStream, tmpMAddr);
            }
            return 0;
        }
        else
        {
            log.write("Listen -> received a packet with an invalid start or end from %s.", tmpMAddr.getAddress().c_str());
            return -1;
        }
#else
#endif // defined
    }

    // Function which processes the known packet types and responds accordingly
    // Takes as parameters the packet buffer and the address that it was sent from
    errno_t ProcessPacket(const std::string& packetStream, MachineAddress m_address)
    {
        // Construct our mighty packet class
        Packet read_packet(packetStream);

        // Fetch the first byte
        int8_t packet_type = read_packet.ReadByte();
        errno_t wasAlreadyProcessed = 0;

        // Error checking...
        if(packet_type == -1)
        {
            log.write("ProcessPacket -> Received packet with invalid start, first byte is -1.");
            return -1;
        }

        // Pass execution control flow to the plugins
        if(!plugin_list.empty())
        {
            for(auto& plugin : plugin_list)
            {
                if((plugin->get_flag(FLAG_CONTROL_CALLBACKS)))
                {
                    wasAlreadyProcessed = plugin->alterCallbacks(m_address, read_packet, packet_type);
                }
            }
        }

        // Now comes the hard part, dealing with every packet type...
        switch(packet_type)
        {
            case PacketTypes::CLIENT_CONNECTION_REQUEST:

                // Let's just make sure we are on the same trail of thought as intended
                // We must either be a server or a peer..
                if((this->connectionType == CONNECTION_TYPE::TYPE_CLIENT) || (this->connectionType == CONNECTION_TYPE::TYPE_UNKNOWN))
                {
                    log.write("ProcessPacket -> received CLIENT_CONNECTION_REQUEST but we are not a server?");
                    return -1;
                }

                // We are a server and the client wants to connect to us. We must first check that we are running the same
                // ... version of the library
                if(client_addresses.size() < MAX_CLIENTS)
                {
                    // Read the bytes and see if they match our versions
                    if(read_packet.ReadByte() == NETLIB_VERSION_LOWER && read_packet.ReadByte() == NETLIB_VERSION_UPPER)
                    {
                        // They do! Log the message
                        log.write("ProcessPacket -> Received new connection from %s. Connection accepted.", m_address.getAddress().c_str());

                        // Add the client in our address book and create his statistics instance
                        client_addresses.push_back(m_address);
                        client_statistics[m_address] = NetNodeStatistics();

                        // Let him know that we are a generous god and we accepted him in our cult
                        SendConnectionAccept(m_address);
                        break;
                    }
                    else // Treat the library-differing conditions. Maybe we should let the poor fella know?
                    {
                        log.write("ProcessPacket -> Received new connection from %s. Libraries differ, connection denied.");
                        return -1;
                    }
                }
                else // Treat the case when the server is full. Again, maybe we should let the poor fella know?
                {
                    log.write("ProcessPacket -> received connection request, server is full. Connection denied.");
                    return -1;
                }
                break;

                // In this case, we are a client and the server has informed us that they have
                // ... accepted our connection request, deal with it accordingly
            case PacketTypes::SERVER_ACCEPT_REQUEST:

                // First make sure that we are a client or a peer..
                if((this->connectionType == CONNECTION_TYPE::TYPE_SERVER) || (this->connectionType == CONNECTION_TYPE::TYPE_UNKNOWN))
                {
                    // Something went wrong, debug message...
                    log.write("ProcessPacket -> received a SERVER_ACCEPT_REQUEST but we are not clients?");
                    return -1;
                }

                // All good, write our success in the history books
                log.write("Server has accepted our connection");

                // Update the connection status
                this->connectionStatus = CONNECTION_STATUS::CLIENT_CONNECTED;

                // Update the server address to the current machine address
                server_address = m_address;

                // Ping the server to let him know we are staying alive
                PingServer();
                break;

                // In this case, we are a client and we have received a ping from the server...
            case PacketTypes::CLIENT_PING:

                // We must either be a client or a peer..
                if((this->connectionType == CONNECTION_TYPE::TYPE_SERVER) || (this->connectionType == CONNECTION_TYPE::TYPE_UNKNOWN))
                {
                    log.write("ProcessPacket -> received CLIENT_PING but we are not a server?");
                    return -1;
                }

#ifdef DEBUG_PINGS
            log.write("Elapsed send time: %llu | Elapsed receive time: %llu | Latency: %llu", server_statistics.GetElapsedSendTime(), server_statistics.GetElapsedReceiveTime(), server_statistics.GetLatency());
            log.write("Received %u pings so far.", server_statistics.GetPingsReceived());
#endif // DEBUG_PINGS

                // Increase the pings that we have received
                server_statistics.IncreasePingsReceived();

                // Return the favour by pinging the server back
                PingServer();

#ifdef DEBUG_PINGS
                log.write("Sent %u pings so far.", server_statistics.GetPingsSent());
#endif // DEBUG_PINGS
                break;

                // In this case, we are a server and we have received a ping from a client...
            case PacketTypes::SERVER_PING:

                // We must either be a server or a peer..
                if((this->connectionType == CONNECTION_TYPE::TYPE_CLIENT) || (this->connectionType == CONNECTION_TYPE::TYPE_UNKNOWN))
                {
                    log.write("ProcessPacket -> received SERVER_PING but we are not a server?");
                    return -1;
                }

#ifdef DEBUG_PINGS
            log.write("[%s] Elapsed send time: %llu | Elapsed receive time: %llu | Latency: %llu", m_address.getAddress().c_str(), client_statistics[m_address].GetElapsedSendTime(), client_statistics[m_address].GetElapsedReceiveTime(), client_statistics[m_address].GetLatency());
            log.write("Received %u pings so far from %s.", client_statistics[m_address].GetPingsReceived(), m_address.getAddress().c_str());
#endif // DEBUG_PINGS

                // Increase the pings that we have received from the client
                client_statistics[m_address].IncreasePingsReceived();

                // Ping him back!
                PingClient(m_address);

#ifdef DEBUG_PINGS
                log.write("Sent %u pings so far to %s.", client_statistics[m_address].GetPingsSent(), m_address.getAddress().c_str());
#endif // DEBUG_PINGS
                break;

                // In his case, we are a server and a client has informed us of his departure.
            case PacketTypes::CLIENT_DISCONNECTION:

                // We must either be a server or a peer..
                if((this->connectionType == CONNECTION_TYPE::TYPE_CLIENT) || (this->connectionType == CONNECTION_TYPE::TYPE_UNKNOWN))
                {
                    log.write("ProcessPacket -> received CLIENT_DISCONNECTION but we are not a server?");
                    return -1;
                }

                // Handle his demise and assume that he quit...
                // Should probably CHANGE this..
                this->DisconnectClient(m_address, DISCONNECT_REASONS::DISCONNECT_QUIT);
                break;

                // I don't even know what's going on at this moment...
            default:

                // Make sure the message wasn't already processed by a plugin
                if(wasAlreadyProcessed != 0)
                    return 0;

                log.write("ProcessPacket -> encountered unknown packet. first byte is: %i", packet_type);
                return -1;
        }

        // We have got to a point where the end-user needs to treat this specific message
        // Do as they wish
        if(this->eventCallback != nullptr)
            this->eventCallback(packet_type, m_address);

        return 0;
    }

    // In this case, we are a client and we wish to connect to a server...
    errno_t SendConnectionRequest()
    {
        // We must either be a client or a peer..
        if((this->connectionType == CONNECTION_TYPE::TYPE_SERVER) || (this->connectionType == CONNECTION_TYPE::TYPE_UNKNOWN))
        {
            log.write("SendConnectionRequest -> required to send the connection request packet but we are not a client?");
            return -1;
        }

        // Create the connection packet
        // Structure it like this: BYTE BYTE BYTE
        // 1st byte: Packet identifier
        // 2nd byte: Netlib lower version
        // 3rd byte: Netlib upper version
        Packet connectionPacket;
        connectionPacket.WriteByte((int8_t)PacketTypes::CLIENT_CONNECTION_REQUEST);
        connectionPacket.WriteByte(NETLIB_VERSION_LOWER);
        connectionPacket.WriteByte(NETLIB_VERSION_UPPER);

        // Verify the integrity of our packet...
        int8_t packet_analyze_integrity = connectionPacket.VerifyIntegrity();

        // 3 means good, the statement is true for all things in the universe...
        if(packet_analyze_integrity != 3)
        {
            log.write("SendConnectionRequest -> error encountered while trying to build the packet, integrity check failed with error %d", packet_analyze_integrity);
            return -1;
        }

        // Gracefully broadcast the packet and handle any errors that might arise
        errno_t iResult = BroadcastPacket(connectionPacket.getStream(), PACKET_FLAGS::FLAG_UNRELIABLE);
        if(iResult != NO_ERROR)
        {
            log.write("SendConnectionRequest -> error encountered while sending the packet.");
            return -1;
        }

        return 0;
    }

    // We are a server and we have decided to let the mortals join our kingdom
    errno_t SendConnectionAccept(MachineAddress m_address)
    {
        // We must either be a server or a peer..
        if((this->connectionType == CONNECTION_TYPE::TYPE_CLIENT) || (this->connectionType == CONNECTION_TYPE::TYPE_UNKNOWN))
        {
            log.write("SendConnectionAccept -> required to accept the connection request packet but we are not a server?");
            return -1;
        }

        // Create the connection accept packet
        // Structure it like this: BYTE BYTE BYTE
        // 1st byte: Packet identifier
        // 2nd byte: Netlib lower version
        // 3rd byte: Netlib upper version
        Packet connectionPacket;
        connectionPacket.WriteByte((int8_t)PacketTypes::SERVER_ACCEPT_REQUEST);
        connectionPacket.WriteByte(NETLIB_VERSION_LOWER);
        connectionPacket.WriteByte(NETLIB_VERSION_UPPER);

        // Verify our packet's integrity
        int8_t packet_analyze_integrity = connectionPacket.VerifyIntegrity();

        // Check against the best digit in the universe
        if(packet_analyze_integrity != 3)
        {
            log.write("SendConnectionAccept -> error encountered while trying to build the packet, integrity check failed with error %d", packet_analyze_integrity);
            return -1;
        }

        // Send the packet and handle any errors
        errno_t iResult = SendPacket(connectionPacket.getStream(), m_address, PACKET_FLAGS::FLAG_UNRELIABLE);
        if(iResult != 0)
        {
            log.write("SendConnectionAccept -> error encountered while sending the packet. error: %d\n", WSAGetLastError());
            return -1;
        }

        return 0;
    }

    // CLIENT SPECIFIC FUNCTION
    void DisconnectFromServer(DISCONNECT_REASONS reasonOfDisconnect)
    {
        // We must either be a client or a peer..
        if((this->connectionType == CONNECTION_TYPE::TYPE_SERVER) || (this->connectionType == CONNECTION_TYPE::TYPE_UNKNOWN))
        {
            log.write("DisconnectFromServer -> requested to disconnect from the server but we are not a client?");
            return;
        }

        // Clear the server statistics
        server_statistics.clear();

        // Clear the server machine address
        server_address.clear();

        // Assign the correct connection status
        switch(reasonOfDisconnect)
        {
            case DISCONNECT_REASONS::DISCONNECT_KICK:
                this->connectionStatus = CONNECTION_STATUS::CLIENT_KICKED;
                break;

            case DISCONNECT_REASONS::DISCONNECT_QUIT:
                this->connectionStatus = CONNECTION_STATUS::CLIENT_DISCONNECTED;
                break;

            case DISCONNECT_REASONS::DISCONNECT_TIMEOUT:
                this->connectionStatus = CONNECTION_STATUS::CLIENT_DISCONNECTED;
                break;

            case DISCONNECT_REASONS::DISCONNECT_CRASH:
                this->connectionStatus = CONNECTION_STATUS::CLIENT_DISCONNECTED;
                break;

            default:
                this->connectionStatus = CONNECTION_STATUS::CLIENT_DISCONNECTED;
                break;
        }

        // Connection was lost, time to clean up or retry
        log.write("ProcessDisconnect called to disconnect from the server. Reason: %d", (int8_t)reasonOfDisconnect);
    }

    // SERVER SPECIFIC FUNCTION
    void DisconnectClient(MachineAddress clientAddress, DISCONNECT_REASONS reasonOfDisconnect)
    {
        // We must either be a server or a peer..
        if((this->connectionType == CONNECTION_TYPE::TYPE_CLIENT) || (this->connectionType == CONNECTION_TYPE::TYPE_UNKNOWN))
        {
            log.write("DisconnectFromServer -> requested to disconnect a client from the server but we are not a server?");
            return;
        }

        // Create a temporary packet storage
        Packet tmpPacket;

        switch(reasonOfDisconnect)
        {
            case DISCONNECT_REASONS::DISCONNECT_KICK:
                tmpPacket.WriteByte((int8_t)PacketTypes::SERVER_KICK);

                // THIS SHOULD REALLY BE CHANGED
                this->SendPacket(tmpPacket.getStream(), clientAddress, PACKET_FLAGS::FLAG_UNRELIABLE);
                // now we don't really care about the return value because
                // this is what we're going to do anyway, right?

                // Empty the local information about the client
                EmptyClient(clientAddress);
                break;

            case DISCONNECT_REASONS::DISCONNECT_QUIT:
                EmptyClient(clientAddress);
                break;

            case DISCONNECT_REASONS::DISCONNECT_TIMEOUT:
                // Empty the client and wait for a reconnect or just clean?
                // I'll go with clean and maybe change my mind in the future
                EmptyClient(clientAddress);
                break;

            default:
                EmptyClient(clientAddress);
                break;
        }

        // Announce the disconnect
        log.write("ProcessDisconnect called to disconnect the server from client %s. Reason: %d", clientAddress.getAddress().c_str(), (int8_t)reasonOfDisconnect);
    }

    // In this case, we are a client which wants to ping the server
    void PingServer()
    {
        // We must either be a client or a peer..
        if((this->connectionType == CONNECTION_TYPE::TYPE_SERVER) || (this->connectionType == CONNECTION_TYPE::TYPE_UNKNOWN))
        {
            log.write("PingServer -> requested to ping the server but we are not a client?");
            return;
        }

        // Construct our ping packet
        // Structure is as follows: BYTE
        // 1st BYTE: Packet identifier
        Packet pingPacket;
        pingPacket.WriteByte((int8_t)PacketTypes::SERVER_PING);

        // Verify our packet's integrity and treat the error case when the integrity is not equal to 3
        int8_t packet_analyze_integrity = pingPacket.VerifyIntegrity();
        if(packet_analyze_integrity != 3)
        {
            log.write("PingServer -> error encountered while trying to build the packet, integrity check failed with error %d", packet_analyze_integrity);
            return;
        }

        // Broadcast our beautiful packet to the open world and treat the error message
        auto result = this->BroadcastPacket(pingPacket.getStream(), PACKET_FLAGS::FLAG_UNRELIABLE);
        if(result != -1)
            this->server_statistics.IncreasePingsSent();
    }

    // In this case we are a server which wishes to ping one of it's clients...
    void PingClient(size_t clientID)
    {
        // We must either be a server or a peer..
        if((this->connectionType == CONNECTION_TYPE::TYPE_CLIENT) || (this->connectionType == CONNECTION_TYPE::TYPE_UNKNOWN))
        {
            log.write("PingClient -> requested to ping a client but we are not a server?");
            return;
        }

        // Construct our ping packet
        // Structure is: BYTE
        // 1st byte: Packet identifier of type CLIENT_PING
        Packet pingPacket;
        pingPacket.WriteByte((int8_t)PacketTypes::CLIENT_PING);

        // Verify our packet's integrity and check against sqrt(9)
        int8_t packet_analyze_integrity = pingPacket.VerifyIntegrity();
        if(packet_analyze_integrity != 3)
        {
            log.write("PingClient -> error encountered while trying to build the packet, integrity check failed with error %d", packet_analyze_integrity);
            return;
        }

        // Send our packet to the client and increment our pings sent
        auto result = this->SendPacket(pingPacket.getStream(), client_addresses.at(clientID), PACKET_FLAGS::FLAG_UNRELIABLE);
        if(result != -1)
            this->client_statistics[client_addresses.at(clientID)].IncreasePingsSent();
    }

    // In this case we are a server which wishes to ping one of it's clients...
    void PingClient(MachineAddress& clientAddress)
    {
        // We must either be a server or a peer..
        if((this->connectionType == CONNECTION_TYPE::TYPE_CLIENT) || (this->connectionType == CONNECTION_TYPE::TYPE_UNKNOWN))
        {
            log.write("PingClient -> requested to ping a client but we are not a server?");
            return;
        }

        // Construct our ping packet
        // Structure is: BYTE
        // 1st byte: Packet identifier of type CLIENT_PING
        Packet pingPacket;
        pingPacket.WriteByte((int8_t)PacketTypes::CLIENT_PING);

        // Verify our packet's integrity and check against 8 % 5
        int8_t packet_analyze_integrity = pingPacket.VerifyIntegrity();
        if(packet_analyze_integrity != 3)
        {
            log.write("PingClient -> error encountered while trying to build the packet, integrity check failed with error %d", packet_analyze_integrity);
            return;
        }

        // Send our packet to the client and increment our pings sent
        auto result = this->SendPacket(pingPacket.getStream(), clientAddress, PACKET_FLAGS::FLAG_UNRELIABLE);
        if(result != -1)
            this->client_statistics[clientAddress].IncreasePingsSent();
    }

    // Treat the gracious demise of one of our clients
    errno_t EmptyClient(const MachineAddress& m_address)
    {
        // We must either be a server or a peer..
        if((this->connectionType == CONNECTION_TYPE::TYPE_CLIENT) || (this->connectionType == CONNECTION_TYPE::TYPE_UNKNOWN))
        {
            log.write("EmptyClient -> requested to empty a client instance but we are not a server?");
            return -1;
        }

        bool wasFound = false;
        for(auto it = client_addresses.begin(); it != client_addresses.end(); ++it)
        {
            if((*it) == m_address)
            {
                wasFound = true;
                client_addresses.erase(it);
                break;
            }
        }

        auto number_of_elements_erased = client_statistics.erase(m_address);

        if(wasFound && !number_of_elements_erased)
            return -1;
        if(!wasFound && number_of_elements_erased)
            return -2;

        return 0;
    }

    // A very poorly crafted function to monitor the well-being of the universe
    // If the last ping received is older than (5) seconds, disconnect.
    errno_t ConnectionStatusCheck()
    {
        // We must be known!
        if(this->connectionType == CONNECTION_TYPE::TYPE_UNKNOWN)
        {
            log.write("ConnectionStatusCheck -> requested to do a connection status check but we are undefined? Connection type is TYPE_UNKNOWN.");
            return -1;
        }

        // Store some relevant variables to calculate the time difference
        int64_t system_time_last_received_reference, system_time_now = Utils::GetCurrentSysTime();

        // Store the NetNodeStatistics temporary instance
        NetNodeStatistics clientTmpStatistics;

        // Check if the server is deployed
        if (!((this->connectionStatus == CONNECTION_STATUS::SERVER_DEPLOYED) || (this->connectionStatus == CONNECTION_STATUS::CLIENT_CONNECTED)))
            return -1;

        // Handle the many (2) possible scenarios
        // We check against the timeout limit and process a disconnect
        // Will probably make the number into a NetPeer variable
        switch(this->connectionType)
        {
            case CONNECTION_TYPE::TYPE_CLIENT:
                system_time_last_received_reference = server_statistics.GetLastReceiveTime();

                if( ((system_time_now - system_time_last_received_reference) > (5 * 1000000)) && server_statistics.GetPingsReceived() > 10 )
                {
                    this->DisconnectFromServer(DISCONNECT_REASONS::DISCONNECT_TIMEOUT);
                }
                return 1;

            case CONNECTION_TYPE::TYPE_SERVER:
                for(MachineAddress& client : this->client_addresses)
                {
                    clientTmpStatistics = this->client_statistics[client];
                    system_time_last_received_reference = clientTmpStatistics.GetLastReceiveTime();

                    if( ((system_time_now - system_time_last_received_reference) > (5 * 1000000)) && client_statistics[client].GetPingsReceived() > 10)
                    {
                        this->DisconnectClient(client, DISCONNECT_REASONS::DISCONNECT_TIMEOUT);
                    }
                }
                return 2;

            default:
                return -2;
        }

        return 0;
    }
};
