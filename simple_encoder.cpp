#include "simple_encoder.h"
#include <stdio.h>

// Process every byte of the integer and return a string with these bytes
std::string SimpleEncoder::encode(int a)
{
    char b[4] = {0};
    b[3] = (a >> 24) & 0xFF;
    b[2] = (a >> 16) & 0xFF;
    b[1] = (a >> 8) & 0xFF;
    b[0] = a & 0xFF;
    return std::string(b,4);
}

// Process every byte of the 16-bit integer and return a string with these bytes
std::string SimpleEncoder::encode(int16_t a)
{
    char b[2] = {0};

    b[1] = (a >> 8) & 0xFF;
    b[0] = a & 0xFF;
    return std::string(b,2);
}

// Process every byte of the 64-bit integer and return a string with these bytes
std::string SimpleEncoder::encode(int64_t a)
{
    char b[8] = {0};
    b[7] = (a >> 56) && 0xFF;
    b[6] = (a >> 48) & 0xFF;
    b[5] = (a >> 40) & 0xFF;
    b[4] = (a >> 32) & 0xFF;
    b[3] = (a >> 24) & 0xFF;
    b[2] = (a >> 16) & 0xFF;
    b[1] = (a >> 8) & 0xFF;
    b[0] = a & 0xFF;
    return std::string(b,8);
}

// Process every byte of the float and return a string with these bytes
 std::string SimpleEncoder::encode(float a)
 {
     char b[4] = {0};
     b[3] = (*reinterpret_cast<uint32_t*>(&a) >> 24) & 0xFF;
     b[2] = (*reinterpret_cast<uint32_t*>(&a) >> 16) & 0xFF;
     b[1] = (*reinterpret_cast<uint32_t*>(&a) >> 8) & 0xFF;
     b[0] = *reinterpret_cast<uint32_t*>(&a) & 0xFF;

    return std::string(b,4);
}

// Create a string which writes the size and the contents
std::string SimpleEncoder::encode(const std::string& str)
{
    int a = static_cast<int>(str.size());
    if( a < 0 ) { printf("encode_str: string length is invalid\n"); return std::string(); }

    char b[4] = {0};
    b[3] = (a >> 24) & 0xFF;
    b[2] = (a >> 16) & 0xFF;
    b[1] = (a >> 8) & 0xFF;
    b[0] = a & 0xFF;

    std::string ret_buf;
    ret_buf += std::string(b, 4);
    ret_buf += str;
    return ret_buf;
 }

 // Encode a byte. Easy as it sounds
 int8_t SimpleEncoder::encode(int8_t byte_to_encode)
 {
     return byte_to_encode & 0xFF;
 }

 // Perform the reverse process, no checking for endianness :/
 float SimpleEncoder::decode_flt(const std::string& flt)
 {
     float tmp = *(float*)(flt.c_str());
     return tmp;
 }

// Perform the reverse process, no checking for endianness :/
 int SimpleEncoder::decode_int(const std::string& integ)
 {
     int tmp = *(int*)(integ.c_str());
     return tmp;
 }

// Perform the reverse process, no checking for endianness :/
 int16_t SimpleEncoder::decode_int16(const std::string& integ)
 {
     int16_t tmp = *(int16_t*)(integ.c_str());
     return tmp;
 }

// Perform the reverse process, no checking for endianness :/
 int64_t SimpleEncoder::decode_int64(const std::string& integ)
 {
     int64_t tmp = *(int64_t*)(integ.c_str());
     return tmp;
 }

  // Perform the reverse process, no checking for endianness :/
 std::string SimpleEncoder::decode_str(const std::string& str)
 {
     if(str.size() < 4) { printf("decode_str: string length is invalid\n"); return std::string(); }

     char buf[4];
     for(int i = 0; i < 4; ++i)
        buf[i] = str[i];

     int strsize = *(int*)(buf);
     if(strsize <= 0) { printf("decode_str: string has invalid size in the packet\n"); return std::string(); }

     std::string retbuf = std::string(static_cast<unsigned int>(strsize), '\0');
     for(int j = 4; j < strsize+4; ++j)
         retbuf[j - 4] = str[j];
     return retbuf;
 }

  // Perform the reverse process, no checking for endianness :/
 int8_t SimpleEncoder::decode_byte(const std::string& str)
 {
     return *(int8_t*)str.c_str();
 }
