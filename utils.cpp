#include "utils.h"

#ifdef _WIN32
#include <windows.h>

#if defined(_WIN64) && (_WIN32_WINNT < 0x0600)
// We need this for the GetTickCount64() function
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif // _SQ64

#else
#include <time.h>
#endif
#include <cstdlib>
#include <codecvt>
#include <cassert>

// This specific file contains all the gimmicks used to make this library awesome (I hope!)

// This function slices a string from start to end
std::string Utils::slice(std::string str_to_slice, size_t start_idx, size_t end_idx)
{
       if(start_idx > end_idx)
       {
           perror("Error in Utils::slice: end idx is lower than start idx.\n");
           return std::string();
       }

       if(end_idx > str_to_slice.size())
       {
           perror("Error in Utils::slice: end idx is greater than string size.\n");
           return std::string();
       }

       std::string tmp(end_idx - start_idx + 1, '\0');
       for(size_t i = start_idx; i < end_idx; ++i)
       {
           tmp[i - start_idx] = str_to_slice[i];
       }
       return tmp;
}

// Slice a string from a start-point to an end-point and make sure that the end index does not overflow the buffer
std::string Utils::slicex(const char* str_to_slice, size_t strsize, size_t start_idx, size_t end_idx)
{
       if(start_idx > end_idx)
       {
           perror("Error in Utils::slice: end idx is lower than start idx.\n");
           return std::string();
       }

       if(end_idx > strsize)
       {
           perror("Error in Utils::slice: end idx is greater than string size.\n");
           return std::string();
       }

       std::string tmp(end_idx - start_idx, '\0');
       for(size_t i = start_idx; i < end_idx; ++i)
       {
           tmp[i - start_idx] = str_to_slice[i];
       }
       return tmp;
}

// Convert an integer to a base-10 string representation
std::string Utils::ToDecimalString(int tmp)
{
    char buf[32];
    snprintf(buf, 32, "%d", tmp);
    return std::string(buf);
}

// This returns the frequency of the CPU
#if defined(_WIN32) || defined(WIN32)
inline LARGE_INTEGER GetFrequency()
{
	LARGE_INTEGER frequency;
	QueryPerformanceFrequency(&frequency);
	return frequency;
}

// ... and this returns the time in microseconds (big thanks to SLC!)
int64_t Utils::GetCurrentSysTime()
{
		// Force the following code to run on first core
		// (see http://msdn.microsoft.com/en-us/library/windows/desktop/ms644904(v=vs.85).aspx)
		HANDLE current_thread = GetCurrentThread();
		DWORD_PTR previous_mask = SetThreadAffinityMask(current_thread, 1);

		// Get the frequency of the performance counter
		// (it is constant across the program lifetime)
		static const LARGE_INTEGER frequency = GetFrequency();

		// Get the current time
		LARGE_INTEGER time;
		QueryPerformanceCounter(&time);

		// Restore the thread affinity
		SetThreadAffinityMask(current_thread, previous_mask);

		// Return the current time as microseconds
		return static_cast<int64_t>(1000000LL * time.QuadPart / frequency.QuadPart);
}




#else

// Process our UNIX implementation (again, big ups to SLC!)
int64_t Utils::GetCurrentSysTime()
{
	// POSIX implementation
	timespec time;
	clock_gettime(CLOCK_MONOTONIC, &time);
	return static_cast<int64_t>(uint64_t(time.tv_sec) * 1000000 + time.tv_nsec / 1000);
}
#endif

char * CompatibilitySafety::safe_strncpy(char * dest, const char * src, size_t count)
{
	#ifdef _MSC_VER
	#pragma warning(push)
	#pragma warning(disable : 4996)
	strncpy(dest, src, count);
	dest[count - 1] = '\0';
	#pragma warning(pop)

	return dest;
	#else
	return strncpy(dest, src, count);
	#endif
}

tm CompatibilitySafety::safe_localtime(const time_t * timer)
{
	#ifdef _MSC_VER
	struct tm buf;
	errno_t error = localtime_s(&buf, timer);

	assert(error != 0);

	return buf;
	#else
	return *localtime(timer);
	#endif
}
