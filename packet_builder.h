#pragma once
#include "utils.h"
#include "simple_encoder.h"

#include <string>
#include <stdio.h>
#include <stdint.h>
#include <vector>
#include <functional>
#include <cstdarg>
#include <cstdio>

#define SIZEOF_FLOAT    4

#define SIZEOF_INT8     1
#define SIZEOF_INT16    2
#define SIZEOF_INT32    4
#define SIZEOF_INT64    8

#define SIZEOF_BYTE     1

class Packet
{
    enum OpenMode
    {
        MODE_READ 	= 0,
        MODE_WRITE 	= 1,
        MODE_UNKNOWN 	= 2
    };

    enum PacketContentTypes
    {
        TYPE_INT = 1,
        TYPE_FLOAT = 2,
        TYPE_STR = 4,
        TYPE_BYTE = 8,
        TYPE_INT16 = 16,
        TYPE_INT64 = 32,
        TYPE_UNKNOWN = 64
    };

    enum InconsistencyError
    {
        ERROR_INVALID_START,
        ERROR_MISSING_IDENTIFIER,
        ERROR_INVALID_CONTENT,
        VALID
    };

    std::string packetStream;
    size_t positionInStream;
    OpenMode openMode;

    std::function<void(const char*)> error_reporter;

public:
    Packet(std::function<void(const char*)> tmpErrorRep) :
        packetStream(),
        positionInStream(0),
        openMode(MODE_UNKNOWN),
        error_reporter(tmpErrorRep)
    {
    }

    Packet() :
        packetStream(),
        positionInStream(0),
        openMode(MODE_UNKNOWN),
        error_reporter(0)
    {
    }

    ~Packet()
    {
        packetStream.clear();
        positionInStream = 0;
        openMode = MODE_UNKNOWN;
    }

    Packet(const std::string& stream, std::function<void(const char*)> tmpErrorReporter)
    {
        error_reporter = tmpErrorReporter;
        packetStream = stream;
        positionInStream = 0;
        openMode = MODE_READ;
    }

    explicit Packet(const std::string& stream)
    {
        error_reporter = 0;
        packetStream = stream;
        positionInStream = 0;
        openMode = MODE_READ;
    }

    OpenMode getOpenMode()
    {
        return openMode;
    }

    explicit operator std::string ()
    {
        return packetStream;
    }

    void clear()
    {
        this->packetStream.clear();
        this->positionInStream = 0;
        this->openMode = MODE_UNKNOWN;
        this->error_reporter = nullptr;
    }

    void reportError(const char* fmt, ...)
    {
        if(!error_reporter)
            return;

        va_list varg;
        va_start(varg, fmt);
        char buf[512];
        vsnprintf(buf, 512, fmt, varg);
        va_end(varg);

        error_reporter(buf);
    }

    std::string getStream()
    {
        return packetStream;
    }

    void resetPosition()
    {
        positionInStream = 0;
    }

    bool IsIdentifierValid(PacketContentTypes id)
    {
        switch(id)
        {
        case PacketContentTypes::TYPE_INT: case PacketContentTypes::TYPE_FLOAT: case PacketContentTypes::TYPE_STR: case PacketContentTypes::TYPE_BYTE:
        case PacketContentTypes::TYPE_INT16: case PacketContentTypes::TYPE_INT64:
            return true;

        default:
            return false;
        }
    }

    void Write(int toWrite)
    {
        if(openMode == MODE_READ)
        {
            reportError("Error in Packet::Write (int32_t): Cannot write when opened for reading.\n");
            return;
        }

        if(openMode == MODE_UNKNOWN)
            openMode = MODE_WRITE;

        std::string tmp = SimpleEncoder::encode(toWrite);

        packetStream += (uint8_t)PacketContentTypes::TYPE_INT;
        packetStream += tmp;

        positionInStream += tmp.size() + 1;
    }

    void Write(int16_t toWrite)
    {
        if(openMode == MODE_READ)
        {
            reportError("Error in Packet::Write (int16_t): Cannot write when opened for reading.\n");
            return;
        }

        if(openMode == MODE_UNKNOWN)
            openMode = MODE_WRITE;

        std::string tmp = SimpleEncoder::encode(toWrite);

        packetStream += (uint8_t)PacketContentTypes::TYPE_INT;
        packetStream += tmp;

        positionInStream += tmp.size() + 1;
    }

    void Write(int64_t toWrite)
    {
        if(openMode == MODE_READ)
        {
            reportError("Error in Packet::Write (int64_t): Cannot write when opened for reading.\n");
            return;
        }

        if(openMode == MODE_UNKNOWN)
            openMode = MODE_WRITE;

        std::string tmp = SimpleEncoder::encode(toWrite);

        packetStream += (uint8_t)PacketContentTypes::TYPE_INT;
        packetStream += tmp;

        positionInStream += tmp.size() + 1;
    }

    void Write(float toWrite)
    {
        if(openMode == MODE_READ)
        {
            reportError("Error in Packet::Write: Cannot write when opened for reading.\n");
            return;
        }

        if(openMode == MODE_UNKNOWN)
            openMode = MODE_WRITE;

        std::string tmp = SimpleEncoder::encode(toWrite);

        packetStream += (uint8_t)PacketContentTypes::TYPE_FLOAT;
        packetStream += tmp;

        positionInStream += tmp.size() + 1;
    }

    void Write(std::string str)
    {
        if(openMode == MODE_READ)
        {
            reportError("Error in Packet::Write: Cannot write when opened for reading.\n");
            return;
        }

        if(openMode == MODE_UNKNOWN)
            openMode = MODE_WRITE;

        std::string tmp = SimpleEncoder::encode(str);

        packetStream += (uint8_t)PacketContentTypes::TYPE_STR;
        packetStream += tmp;

        positionInStream += tmp.size() + 1;
    }

    void WriteByte(int8_t toWrite)
    {
        if(openMode == MODE_READ)
        {
            reportError("Error in Packet::Write: Cannot write when opened for reading.\n");
            return;
        }

        if(openMode == MODE_UNKNOWN)
            openMode = MODE_WRITE;

        char tmp = SimpleEncoder::encode(toWrite);

        packetStream += (uint8_t)PacketContentTypes::TYPE_BYTE;
        packetStream += tmp;

        positionInStream += (SIZEOF_BYTE + 1);
    }

    InconsistencyError VerifyIntegrity()
    {
        // backup values to perform integrity check
        OpenMode tmpOpenMode = openMode;
        openMode = MODE_UNKNOWN;

        size_t tmpPositionStream = positionInStream;
        size_t newPosition = 0;

        if(!IsIdentifierValid((PacketContentTypes) packetStream[newPosition]))
        {
            reportError("Error in Packet::VerifyIntegrity: Illegal starting identifier.\n");
            return InconsistencyError::ERROR_INVALID_START;
        }

        std::string tmpSliceString; int tmpSliceSize;
        for(size_t i = 0; i < packetStream.size(); ++i)
        {
            PacketContentTypes tmpIdentifier = static_cast<PacketContentTypes>(packetStream[i]);
            if(IsIdentifierValid(tmpIdentifier) != true)
            {
                reportError("Error in Packet::VerifyIntegrity: Illegal identifier.\n");
                return InconsistencyError::ERROR_MISSING_IDENTIFIER;
            }

            switch(tmpIdentifier)
            {
            case TYPE_INT:
                i += SIZEOF_INT32;
                break;

            case TYPE_INT16:
                i += SIZEOF_INT16;
                break;

            case TYPE_INT64:
                i += SIZEOF_INT64;
                break;

            case TYPE_FLOAT:
                i += SIZEOF_FLOAT;
                break;

            case TYPE_STR:
                tmpSliceString = Utils::slice(packetStream, i + 2, i + SIZEOF_INT32 + 2);
                tmpSliceSize = *(int*)tmpSliceString.c_str();

                if(tmpSliceSize < 1)
                {
                    reportError("Error in Packet::VerifyIntegrity: Illegal string size found.\n");
                    return InconsistencyError::ERROR_INVALID_CONTENT;
                }

                i = i + 1 + tmpSliceSize;
                break;

            case TYPE_BYTE:
                i += SIZEOF_BYTE;
                break;

            default:
                reportError("Unexpected fatal error in Packet::VerifyIntegrity: Illegal identifier found.\n");
                return InconsistencyError::ERROR_MISSING_IDENTIFIER;
            }
        }
        positionInStream = tmpPositionStream;
        openMode = tmpOpenMode;
        return InconsistencyError::VALID;
    }

    int16_t ReadInt16()
    {
        if(openMode != MODE_READ)
        {
            reportError("Error in Packet::ReadInt: Cannot read when opened for writing.\n");
            return -1;
        }

        if(static_cast<PacketContentTypes>(packetStream[positionInStream ]) != TYPE_INT16)
        {
            reportError("Error in Packet::ReadInt: Next character in stream does not point to an int.\n");
            return -1;
        }

        int16_t toReturn = SimpleEncoder::decode_int16(Utils::slice(packetStream, positionInStream + 1, positionInStream + SIZEOF_INT16 + 1).c_str());
        positionInStream += (SIZEOF_INT16 + 1);
        return toReturn;
    }

    int64_t ReadInt64()
    {
        if(openMode != MODE_READ)
        {
            reportError("Error in Packet::ReadInt: Cannot read when opened for writing.\n");
            return -1;
        }

        if(static_cast<PacketContentTypes>(packetStream[positionInStream ]) != TYPE_INT64)
        {
            reportError("Error in Packet::ReadInt: Next character in stream does not point to an int.\n");
            return -1;
        }

        int64_t toReturn = SimpleEncoder::decode_int64(Utils::slice(packetStream, positionInStream + 1, positionInStream + SIZEOF_INT64 + 1).c_str());
        positionInStream += (SIZEOF_INT64 + 1);
        return toReturn;
    }

    int ReadInt()
    {
        if(openMode != MODE_READ)
        {
            reportError("Error in Packet::ReadInt: Cannot read when opened for writing.\n");
            return -1;
        }

        if(static_cast<PacketContentTypes>(packetStream[positionInStream ]) != TYPE_INT)
        {
            reportError("Error in Packet::ReadInt: Next character in stream does not point to an int.\n");
            return -1;
        }

        int toReturn = SimpleEncoder::decode_int(Utils::slice(packetStream, positionInStream + 1, positionInStream + SIZEOF_INT32 + 1).c_str());
        positionInStream += (SIZEOF_INT32 + 1);
        return toReturn;
    }

    float ReadFloat()
    {
        if(openMode != MODE_READ)
        {
            reportError("Error in Packet::ReadFloat: Cannot read when opened for writing.\n");\
            return -1.0f;
        }

        if(static_cast<PacketContentTypes>(packetStream[positionInStream]) != TYPE_FLOAT)
        {
            reportError("Error in Packet::ReadFloat: Next character in stream does not point to a float.\n");
            return -1.0f;
        }

        float toReturn = SimpleEncoder::decode_flt(Utils::slice(packetStream, positionInStream + 1, positionInStream + SIZEOF_FLOAT + 1).c_str());
        positionInStream += (SIZEOF_FLOAT + 1);
        return toReturn;
    }

    std::string ReadString()
    {
        if(openMode != MODE_READ)
        {
            reportError("Error in Packet::ReadString: Cannot read when opened for writing.\n");\
            return std::string();
        }

        if(static_cast<PacketContentTypes>(packetStream[positionInStream]) != TYPE_STR)
        {
            reportError("Error in Packet::ReadString: Next character in stream does not point to a string.\n");
            return std::string();
        }

        int strsize = SimpleEncoder::decode_int( Utils::slice(packetStream, positionInStream + 1, positionInStream + SIZEOF_INT32 + 1) );

        std::string toReturn = SimpleEncoder::decode_str( Utils::slice(packetStream, positionInStream + 1, positionInStream + SIZEOF_INT32 + 1 + strsize) );
        positionInStream += (SIZEOF_INT32 + 1 + strsize);
        return toReturn;
    }

    int8_t ReadByte()
    {
        if(openMode != MODE_READ)
        {
            reportError("Error in Packet::ReadByte: Cannot read when opened for writing.\n");
            return -1;
        }

        if(static_cast<PacketContentTypes>(packetStream[positionInStream]) != TYPE_BYTE)
        {
            reportError("Error in Packet::ReadByte: Next character in stream does not point to a byte.\n");
            return -1;
        }

        int8_t toReturn = SimpleEncoder::decode_byte(Utils::slice(packetStream, positionInStream + 1, positionInStream + SIZEOF_BYTE + 1).c_str());
        positionInStream += (SIZEOF_BYTE + 1);
        return toReturn;
    }
};
