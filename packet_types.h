#pragma once
#include <stdint.h>

#define NETLIB_VERSION_LOWER 1
#define NETLIB_VERSION_UPPER 0

enum PacketSegments
{
    PACKET_BEGIN = 8,
    PACKET_END = 12
};

namespace PacketTypes
{
    enum TypeOfPacket
    {
        CLIENT_CONNECTION_REQUEST = 1,
        CLIENT_SERVER_REQUEST = 2,
        CLIENT_UPDATE_REQUEST = 3,
        CLIENT_ANSWER = 4,
        CLIENT_PING = 5,
        CLIENT_CUSTOM = 6,
        CLIENT_DISCONNECTION = 7,
        SERVER_ACCEPT_REQUEST = 9,
        SERVER_DENY_REQUEST = 10,
        SERVER_PING = 11,
        SERVER_CUSTOM = 13,
        SERVER_KICK = 14
    };
}
