#include "serializer.h"

#include <cassert>
#include <cstring>

uint8_t is_little_endian()
{
    uint16_t test_num = 1;

    uint8_t* byte_ptr = reinterpret_cast<uint8_t*>(&test_num);
    return byte_ptr[0];
}

template <typename T>
T swap_endians(T object)
{
    T new_object;

    uint8_t* obj_ptr        =      reinterpret_cast<uint8_t*>(&object);
    uint8_t* new_obj_ptr    =      reinterpret_cast<uint8_t*>(&new_object);

    for(uint64_t i = 0; i < sizeof(T); ++i)
    {
        new_obj_ptr[sizeof(T) - i - 1] = obj_ptr[i];
    }

    return new_object;
}

std::vector<uint8_t> swap_endians(std::vector<uint8_t> object)
{
    const uint64_t object_size = object.size();
    std::vector<uint8_t> new_object(object_size, 0);

    for(uint64_t i = 0; i < object_size; ++i)
    {
        new_object.at(object_size - i - 1) = object.at(i);
    }

    return new_object;
}

template <typename T>
std::vector<uint8_t> serialize(T object)
{
    uint64_t object_size = sizeof(T);
    assert(object_size == sizeof(object));

    std::vector<uint8_t> serialized_object(object_size, 0);

    if(is_little_endian() == 1)
        object = swap_endians(object);

    memcpy(serialized_object.data(), &object, object_size);
    return serialized_object;
}

std::vector<uint8_t> serialize(std::vector<uint8_t> object)
{
    const uint64_t object_size = object.size();

    std::vector<uint8_t> serialized_object(object_size, 0);

    if(is_little_endian() == 1)
        serialized_object = swap_endians(object);
    else
        serialized_object = object;

    return serialized_object;
}

template <typename T>
T deserialize(std::vector<uint8_t> serialized_object)
{
    uint64_t object_size = sizeof(T);

    assert(object_size == serialized_object.size());

    T object;
    memcpy(&object, serialized_object.data(), object_size);

    if(is_little_endian() == 1)
        object = swap_endians(object);

    return object;
}

std::vector<uint8_t> deserialize(std::vector<uint8_t> serialized_object)
{
    const uint64_t object_size = serialized_object.size();

    std::vector<uint8_t> object(object_size, 0);

    if(is_little_endian() == 1)
        object = swap_endians(serialized_object);
    else
        object = serialized_object;

    return object;
}
