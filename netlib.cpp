#include "netlib.h"
#include "error_report.h"

#include <cstddef>
#include <stdio.h>

// This library generally handles the transformation of IP addresses from
// ... character arrays to 32-bit representations

// The conversion is quite self-explanatory, you take each byte (that's usually separated by a dot [.])
// ... and you fill an empty 32-bit data-type with it
// i.e: 127.0.0.1 would become 0x7F000001

void customDelete(NetlibPlugin* tmp)
{
    delete tmp;
    tmp = nullptr;
}

#ifdef _WIN32
int8_t inet_pton(int af, const char *src, void *dst)
{
  struct sockaddr_storage ss;
  int size = sizeof(ss);
  char src_copy[INET6_ADDRSTRLEN+1];

  ZeroMemory(&ss, sizeof(ss));
  /* stupid non-const API */
  CompatibilitySafety::safe_strncpy(src_copy, src, INET6_ADDRSTRLEN);
  src_copy[INET6_ADDRSTRLEN] = 0;

  if (WSAStringToAddress(src_copy, af, nullptr, (struct sockaddr *)&ss, &size) == 0) {
    switch(af) {
      case AF_INET:
        *(struct in_addr *)dst = ((struct sockaddr_in *)&ss)->sin_addr;
        return 1;

      case AF_INET6:
        *(struct in6_addr *)dst = ((struct sockaddr_in6 *)&ss)->sin6_addr;
        return 1;

        default:
            return -1;
    }
  }
  return 0;
}

int8_t inet_ntop(int af, const void *src, char *dst, socklen_t size)
{
  struct sockaddr_storage ss;
  socklen_t s = size;

  ZeroMemory(&ss, sizeof(ss));
  ss.ss_family = (short)af;

  switch(af) {
    case AF_INET:
      ((struct sockaddr_in *)&ss)->sin_addr = *(struct in_addr *)src;
      break;
    case AF_INET6:
      ((struct sockaddr_in6 *)&ss)->sin6_addr = *(struct in6_addr *)src;
      break;
    default:
      return 0;
  }
  /* cannot direclty use &size because of strict aliasing rules */
  return (WSAAddressToString((struct sockaddr *)&ss, (DWORD)sizeof(ss), NULL, dst, (LPDWORD)&s) == 0)?
          1 : 0;
}
#endif // _WIN32


std::string NetResolver::GetIPFromSockaddr(NetlibAddrInfo* tmp)
{
    #if defined(_WIN32) || defined(_WIN32_WINNT) || defined(_WINNT_)
    struct sockaddr_in * tmp_sockaddrin = (struct sockaddr_in*)tmp->ai_addr;

    char ip_address[INET_ADDRSTRLEN];
    int8_t retVal;

    retVal = inet_ntop(AF_INET, &tmp_sockaddrin->sin_addr, ip_address, INET_ADDRSTRLEN);

    if(retVal == 0)
        return std::string("F");
    else
        return std::string(ip_address);
   #else
   inet_ntop(AF_INET, &(tmp_sockaddrin->sin_addr), ip_address, INET_ADDRSTRLEN);
   return std::string(ip_address);
   #endif
}

std::string NetResolver::GetIPFromSockaddr(NetLibSockAddrRaw t_raw)
{
    #if defined(_WIN32) || defined(_WIN32_WINNT) || defined(_WINNT_)
    struct sockaddr_in * tmp = (struct sockaddr_in*)&t_raw;
    char ip_address[INET_ADDRSTRLEN];
    int8_t retVal;

    retVal = inet_ntop(AF_INET, &tmp->sin_addr, ip_address, INET_ADDRSTRLEN);

    if(retVal == 0)
        return std::string("F");
    else
        return std::string(ip_address);
   #else
   inet_ntop(AF_INET, &(tmp.sin_addr), ip_address, INET_ADDRSTRLEN);
   return std::string(ip_address);
   #endif
}

std::string NetResolver::GetIPFromSockaddr(NetlibSockaddrIn tmp)
{
    #if defined(_WIN32) || defined(_WIN32_WINNT) || defined(_WINNT_)
    char ip_address[INET_ADDRSTRLEN];
    int8_t retVal;

    retVal = inet_ntop(AF_INET, &tmp.sin_addr, ip_address, INET_ADDRSTRLEN);

    if(retVal == 0)
        return std::string("F");
    else
        return std::string(ip_address);
   #else
   inet_ntop(AF_INET, &(tmp.sin_addr), ip_address, INET_ADDRSTRLEN);
   return std::string(ip_address);
   #endif
}

uint16_t NetResolver::GetPortFromSockaddr(NetlibSockaddrIn tmp)
{
    return static_cast<uint16_t>(ntohs(tmp.sin_port));
}
