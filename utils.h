#pragma once
#include <string>
#include <ctime>

namespace Utils
{
	std::string slice(std::string str_to_slice, size_t start_idx, size_t end_idx);
	std::string slicex(const char* str_to_slice, size_t strsize, size_t start_idx, size_t end_idx);

	std::string ToDecimalString(int tmp);
	int64_t GetCurrentSysTime();
}

namespace CompatibilitySafety
{
	char* safe_strncpy(char* dest, const char* src, size_t count);
	struct tm safe_localtime(const time_t * timer);
}

namespace Bounds
{
	template <typename T>
	T clamp(T val, T min, T max)
	{
		return (val > max) ? max : ((val > min) ? val : min);
	}
}
