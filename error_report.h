#pragma once
#include <fstream>
#include <string>
#include <ctime>
#include <cstdarg>
#include <cstdio>
#include <vector>
#include "utils.h"

enum LogType
{
    LOG_FATAL,
    LOG_ERROR,
    LOG_WARNING,
    LOG_NOTIFICATION,
    LOG_SUCCESS
};

class FileLogger
{

private:
    std::ofstream output_log;
    bool is_active;
    std::string file_path;

public:
    FileLogger()
    :
        output_log(),
        is_active(false),
        file_path()
    {
    }

    FileLogger(std::string path)
    {
        is_active = false;
        output_log.open(path.c_str());

        if(!output_log)
        {
            perror("ERROR: Cannot open log file. File instance is invalid.");
            return;
        }

        if(!output_log.is_open())
        {
            perror("ERROR: Cannot open log file. is_open() returns null.");
            return;
        }
        is_active = true;
        file_path = path;

        this->write("========= BEGINNING LOGGING SESSION =========");
    }

    FileLogger(const char* path)
    {
        is_active = false;
        output_log.open(path);

        if(!output_log)
        {
            printf("ERROR: Cannot open log file. File instance is invalid.\n");
            return;
        }

        if(!output_log.is_open())
        {
            printf("ERROR: Cannot open log file. is_open() returns null.\n");
            return;
        }
        is_active = true;
        file_path = std::string(path);

        this->write("========= BEGINNING LOGGING SESSION =========");
    }

    void Initialize(const char* path)
    {
        is_active = false;
        output_log.open(path);

        if(!output_log)
        {
            printf("ERROR: Cannot open log file. File instance is invalid.\n");
            return;
        }

        if(!output_log.is_open())
        {
            printf("ERROR: Cannot open log file. is_open() returns null.\n");
            return;
        }
        is_active = true;
        file_path = std::string(path);

        this->write("========= BEGINNING LOGGING SESSION =========");
    }

    FileLogger& operator= (const FileLogger& other)
    {
        if(this != &other)
        {
            this->output_log = std::ofstream(other.file_path);
            this->is_active = other.is_active;
        }
        return *this;
    }

    void write(const char* fmt, ...)
    {
        if(!output_log.is_open() || !is_active)
        {
            printf("Error in write, output_log is not open!\n");
            return;
        }

        std::time_t t = std::time(nullptr);
		struct tm tstruct = CompatibilitySafety::safe_localtime(&t);

        char time_buffer[256];
        std::strftime(time_buffer, sizeof(time_buffer), "[%d.%m.%Y %X %A %B %Z]", &tstruct);


        va_list args1;
        va_start(args1, fmt);
        char buf[256];
        vsnprintf(buf, 256, fmt, args1);
        va_end(args1);

        std::string ovrbuf;
        ovrbuf += time_buffer;
        ovrbuf += ' ';
        ovrbuf += buf;
        ovrbuf += '\n';
        output_log.write(ovrbuf.c_str(), ovrbuf.size());
    }

    ~FileLogger()
    {
        is_active = false;
        output_log.close();
    }
};
