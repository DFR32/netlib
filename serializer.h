#ifndef SERIALIZER_H
#define SERIALIZER_H
#include <vector>
#include <cstdint>

uint8_t is_little_endian();

template <typename T>
T swap_endians(T object);

std::vector<uint8_t> swap_endians(std::vector<uint8_t> object);

template <class T>
std::vector<uint8_t> serialize(T object);

template <class T>
T deserialize(std::vector<uint8_t> serialized_object);

std::vector<uint8_t> serialize(std::vector<uint8_t> object);

std::vector<uint8_t> deserialize(std::vector<uint8_t> object);
#endif // SERIALIZER_H
