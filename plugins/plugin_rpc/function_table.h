#pragma once

#include <map>
#include <functional>
#include <string>

// Neat wrapper for the map that we are using to map
// ... function names to actual functions
template<typename T>
class RPCFunctionTable
{
    // Look above!
    std::map<std::string, std::function<T> > RPCMap;

public:

    // Default constructor
    RPCFunctionTable()
        :
            RPCMap()
    {
    }

    // Default destructor
    ~RPCFunctionTable()
    {
    }

    // Return an RPC function by it's name
    errno_t get(std::string funcname, std::function<T>& func)
    {
        if(RPCMap.find(funcname) == RPCMap.end())
            return 0;

        func = RPCMap[funcname];
        return 1;
    }

    // Set/Register an RPC function by it's name
    errno_t set(std::string funcname, std::function<T> func)
    {
        if(RPCMap.find(funcname) != RPCMap.end())
            return 0;

        RPCMap[funcname] = func;
        return 1;
    }

    // Call an RPC function by it's name
    template<typename T2>
    errno_t call(std::string funcname, T2 params)
    {
        if(RPCMap.find(funcname) == RPCMap.end())
            return 0;

        RPCMap[funcname](params);
        return 1;
    }
};
