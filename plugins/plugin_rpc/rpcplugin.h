#pragma once
#include "netlib.h"
#include "function_table.h"

// Enumerate the types of RPC packets that we have to deal with
enum RPC_PACKETS
{
    // START AT 25
    RPC_INVOKE = 25,
    RPC_RETURN = 26
};

// Create our beautiful class
template<typename T>
class RPCPlugin : public NetlibPlugin
{
private:
        // Have a function-lookup table
        RPCFunctionTable<T> fTabl;

public:

    // Default constructor, initialize the plugin with our information
    RPCPlugin() :
        fTabl()
    {
        NetlibPlugin
        (
            "RPC Plugin 1.0",
            "DFR3264",
            1,
            0
        );

        this->set_flag(FLAG_CONTROL_CALLBACKS, 1);
    }

    // Default destructor
    ~RPCPlugin()
    {
    }


    // Process our RPCs
    // Make sure to override the virtual function!
    errno_t alterCallbacks(MachineAddress& m_address, Packet& packet, int8_t& callback_type) override
    {
        std::string tmpFuncStr;
        errno_t retMsg;

        std::string packstream = packet.getStream();

        switch(callback_type)
        {
            case RPC_INVOKE:
                tmpFuncStr = packet.ReadString();

                if(tmpFuncStr == std::string())
                {
                        printf("Invalid string in function name.\n");
                }

                retMsg = this->Call(tmpFuncStr, packet);

                packet.resetPosition();
                return retMsg;

            case RPC_RETURN:
                return 0;

            default:
                return 0;
        }
        return 0;
    }

    // Register an RPC into the function table
    errno_t Register(std::string name, T func)
    {
        return fTabl.set(name, func);
    }

    // Call an RPC function from the table and return whether the call was successful or not
    template<typename T2>
    errno_t Call(std::string name, T2 parameters)
    {
        return fTabl.call(name, parameters);
    }

    // Create an RPC call packet
    Packet createCallPacket(std::string funcname, std::string packetparams)
    {
        // Structure is as follows: BYTE STRING STRING
        // 1st BYTE: Packet identifier of type RPC_INVOKE
        // 2nd STR:  Function name as a string
        // 3rd STR:  Function parameters as a string (can be the stream of a packet)
        Packet packet;
        packet.WriteByte((int8_t)RPC_INVOKE);
        packet.Write(funcname);
        packet.Write(packetparams);

        // Return the packet
        return packet;
    }
};
